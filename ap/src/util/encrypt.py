from cryptography.fernet import Fernet
import getpass
import configparser
from argparse import ArgumentParser


SECRET_KEY = 'secret.key'
CONFIG_FILE = 'src/config/config.ini'


def generate_key():
    """
    Generates a key and save it into a file
    """
    key = Fernet.generate_key()
    with open(SECRET_KEY, "wb") as key_file:
        key_file.write(key)

def load_key():
    """
    Load the previously generated key
    """
    with open(SECRET_KEY, "rb") as key_file:
        key = key_file.read()
    return key

def encrypt_message(message):
    """
    Encrypts a message
    """
    key = load_key()
    encoded_message = message.encode()
    f = Fernet(key)
    encrypted_message = f.encrypt(encoded_message)

    return encrypted_message.decode()

def decrypt_message(encrypted_message):
    """
    Decrypts an encrypted message
    """
    key = load_key()
    f = Fernet(key)
    decrypted_message = f.decrypt(encrypted_message.encode())
    return decrypted_message.decode()


def update_config(section, property, content):
    encrypted_content = encrypt_message(content)

    config = configparser.ConfigParser()
    config.read(CONFIG_FILE)

    config[section][property] = encrypted_content
    with open(CONFIG_FILE, 'w') as configfile:
        config.write(configfile)


def main():
    parser = ArgumentParser(description='update config and encrypt')
    parser.add_argument("-s", "--section", help="config section", dest="section", required=True)
    parser.add_argument("-p", "--property", help="property in section", dest="property", required=True)
    parser.add_argument("-c", "--content", help="your content string", dest="content", required=True)
    args = parser.parse_args()

    update_config(args.section, args.property, args.content)

if __name__ == "__main__":
    main()
