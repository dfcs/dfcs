"""
Author: Howard Ye
Description: This is the role-based security permission check model
"""
from src.db import Base
from flask_security import UserMixin, RoleMixin
from sqlalchemy.orm import relationship, backref
from sqlalchemy import Boolean, DateTime, Column, Integer, String, ForeignKey
from sqlalchemy.types import JSON
import datetime

SCHEMA='DFCS'

class RolesUsers(Base):
    __tablename__ = 'dfap_scy_ur'
    id = Column(Integer(), primary_key=True)
    user_id = Column('user_id', Integer(),ForeignKey('dfap_scy_user.id'))
    role_id = Column('role_id', Integer(),ForeignKey('dfap_scy_role.id'))
    create_dttm = Column(DateTime(timezone=True), default=datetime.datetime.utcnow)
    modify_dttm = Column(DateTime(timezone=True), onupdate=datetime.datetime.utcnow)

class Role(Base, RoleMixin):
    __tablename__ = 'dfap_scy_role'
    id = Column(Integer(), primary_key=True)
    name = Column(String(80),unique=True)
    menu_privileges = Column(JSON())
    description = Column(String(255))
    create_dttm = Column(DateTime(timezone=True), default=datetime.datetime.utcnow)
    modify_dttm = Column(DateTime(timezone=True), onupdate=datetime.datetime.utcnow)

class User(Base, UserMixin):
    __tablename__ = 'dfap_scy_user'
    id = Column(Integer, primary_key=True)
    email = Column(String(255), unique=True)
    username = Column(String(255), unique=True)
    password = Column(String(255))
    last_login_at = Column(DateTime(timezone=True))
    current_login_at = Column(DateTime(timezone=True))
    last_login_ip = Column(String(100))
    current_login_ip = Column(String(100))
    login_count = Column(Integer)
    active = Column(Boolean())
    notify = Column(String(1),default='Y')
    confirmed_at = Column(DateTime(timezone=True))
    expired_at = Column(DateTime(timezone=True))
    create_dttm = Column(DateTime(timezone=True), default=datetime.datetime.utcnow)
    modify_dttm = Column(DateTime(timezone=True), onupdate=datetime.datetime.utcnow)
    roles = relationship('Role', secondary='dfap_scy_ur',
                         backref=backref('dfap_scy_users', lazy='dynamic'))
    def to_json(self):
        return {"id":self.id,
            "email":self.email,
            "username":self.username,
            "notify":self.notify,
            "last_login_at":self.last_login_at,
            "expired_at":self.expired_at,
            "role":self.roles[0].id}

    