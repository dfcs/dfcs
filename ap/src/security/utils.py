"""
Author: Howard Ye
Description: for security init
"""
from flask import current_app, request
from src.db import db_session
from datetime import datetime,timedelta
import logging
import copy
import json
from src.etl.util import get_syscode_data
from src.security.vulnerability import hash_pwd

LOGGER = logging.getLogger('dfap')

def merge_privileges(role_privileges_arr):
  flatten = flatten_privileges_arr(role_privileges_arr)

  result = role_privileges_arr[0]

  for x in flatten.keys():
    p_path = x.split('/')[1:]
    obj = result
    for y in p_path:
      obj = obj[y]
    obj['enable'] = flatten.get(x)
  return result


def flatten_privileges_arr(role_privileges_arr)->dict:
  role_privileges = {}
  for x in role_privileges_arr:
    tmp_role_priviledge = flatten_privileges("",x)
    for x in tmp_role_priviledge.keys():
        if role_privileges.get(x):
            role_privileges[x] = role_privileges[x] or tmp_role_priviledge.get(x)
        else:
            role_privileges[x] = tmp_role_priviledge.get(x)
  return role_privileges

def flatten_privileges(prefix,privileges):
  result = {}

  if isinstance(privileges,dict):
      for k in privileges.keys():
          tmp_values = privileges.get(k)
          if k == 'privileges':
              tmp_values = privileges.get(k)
              for key_x in tmp_values.keys(): 
                  result[prefix+"/privileges/"+key_x] = tmp_values.get(key_x).get("enable")
              return result
          elif isinstance(tmp_values,dict):
              tmp_result = flatten_privileges(prefix+"/"+k,tmp_values)
              for x in tmp_result.keys():
                  result[x] = tmp_result.get(x)
          else:
              pass
  return result

