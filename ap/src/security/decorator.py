"""
Author: Howard Ye
Description: This is security decorator extent the flask security
"""
from functools import wraps
from .utils import flatten_privileges_arr
from src.error_code import DFAPForbiddenException
import collections

def flatten(d, parent_key='', sep='/'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)

def import_user():
    try:
        from flask_security import current_user
        return current_user
    except ImportError:
        raise ImportError(
            'User argument not passed and Flask-Login current_user could not be imported.')

def user_has(priviledge_unit_arr:[], get_user=import_user):
    def wrapper(func):
        @wraps(func)
        def inner(*args, **kwargs):
            current_user = get_user()

            if len(current_user.roles) == 0:
                raise DFAPForbiddenException("Please login first.")
            menu_privileges = current_user.roles[0].menu_privileges
            user_privileges = flatten(menu_privileges)
 
            for x in priviledge_unit_arr:
                if not user_privileges.get(x):
                    raise DFAPForbiddenException("You do not have priviledge to access")
            return func(*args, **kwargs)
        return inner
    return wrapper


def user_is(role, get_user=import_user):
    """
    Takes an role (a string name of either a role or an ability) and returns the function if the user has that role
    """
    def wrapper(func):
        @wraps(func)
        def inner(*args, **kwargs):
            from .models import Role
            current_user = get_user()
            if role in current_user.roles:
                return func(*args, **kwargs)
            raise DFAPForbiddenException("You do not have access")
        return inner
    return wrapper