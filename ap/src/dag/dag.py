import logging


LOGGER = logging.getLogger('dfap')


class Nodes():
    def __init__(self, dep_groups, target_node, records, roots):
        self.dep_groups = dep_groups
        self.target_node = target_node
        self.records = records
        self.roots = roots
        self.error = None # if occurs error
        self.complete_list = list()
        self.data = self._gen_nodes()

    def _create_upstream_node(self, node_id, union_id, nodes):
        """
        方向性是固定的
        左 ---------union--------- 右
        partner -----union--------- children
        """
        is_new_node = True
        if node_id not in nodes:
            nodes[node_id] = {
                'id': node_id,
                'parent_union': union_id,
                'own_unions': [],
                'stream': 'UP'
                }
        else:
            is_new_node = False

        return is_new_node, nodes

    def _create_downstream_node(self, node_id, union_id, nodes):
        """
        方向性是固定的
        左 ---------union--------- 右
        partner -----union--------- children
        """
        if node_id not in nodes:
            nodes[node_id] = {
                'id': node_id,
                'parent_union': union_id,
                'own_unions': [],
                'stream': 'DOWN'
                }
        else:
            pass

        return nodes

    def _upstream_recursive(self, same_layer_nodes, nodes=dict(), layer=1):
        upstream = list(self.dep_groups['upstream']) + [self.target_node]
        flag = 'U'
        idx = 1
        next_layer_nodes = []

        for node_id in same_layer_nodes:
            stream = self.records[node_id]['downstream']

            stream = [i for i in stream if i in upstream]
            for dep_id in stream:
                # 判斷是否cycle
                if dep_id in self.complete_list:
                    LOGGER.warn(f'upstream node_id: {node_id}, dep_id: {dep_id}')
                    self.error = [node_id, dep_id] # 表示node_id -> dep_id發生cycle
                    return nodes

                union_id = f'{layer}{flag}{idx}'
                is_new_node, nodes = self._create_upstream_node(dep_id, union_id, nodes)
                if is_new_node:
                    nodes[node_id]['own_unions'].append(union_id)
                    idx += 1
                else:
                    nodes[node_id]['own_unions'].append(
                        nodes[dep_id]['parent_union']
                    )
            nodes[node_id]['own_unions'] = list(set(nodes[node_id]['own_unions']))
            next_layer_nodes += stream
            if stream:
                self.complete_list.append(node_id)
        layer += 1

        if next_layer_nodes:
            return self._upstream_recursive(set(next_layer_nodes), nodes=nodes, layer=layer)
        else:
            return nodes

    def _create_root_nodes(self):
        nodes = dict()
        roots = self.roots
        for root_id in roots:
            nodes[root_id] = {
                'id': root_id,
                'own_unions': [],
                'stream': 'UP'
                }
        return nodes

    def _downstream_recursive(self, same_layer_nodes, nodes=dict(), layer=1):
        flag = 'D'
        idx = 1
        next_layer_nodes = []

        for node_id in same_layer_nodes:
            stream = self.records[node_id]['downstream']
            for dep_id in stream:

                # 判斷是否cycle
                if dep_id in self.complete_list:
                    # print(f'downstream node_id: {node_id}, dep_id: {dep_id}')
                    self.error = [node_id, dep_id] # 表示node_id -> dep_id發生cycle
                    return nodes
                
                union_id = f'{layer}{flag}{idx}'
                nodes = self._create_downstream_node(dep_id, union_id, nodes)
            if stream:
                # 下游都只有一個own_union
                if 'own_unions' in nodes[node_id] and len(nodes[node_id]['own_unions']) != 0:
                    pass
                else: 
                    nodes[node_id]['own_unions'] = [union_id]
                idx += 1
                next_layer_nodes += stream
                self.complete_list.append(node_id)
        layer += 1

        if next_layer_nodes:
            return self._downstream_recursive(set(next_layer_nodes), nodes=nodes, layer=layer)
        else:
            return nodes

    def _gen_nodes(self):
        """
        {
            "id100101": {
                "id": "id100101",
                "own_unions": [
                    "3U3"
                ],
                "parent_union": "2U11",
                "stream": "UP"
            },...
        }
        """
        merge_nodes = {}

        # 建立target node
        nodes = {
            self.target_node: {
                'id': self.target_node,
                'own_unions': []
            }
        }

        root_nodes = self._create_root_nodes()
        # TODO: 檢查self.error若存在要將own_unions使用upstream
        up_nodes = self._upstream_recursive(self.roots, root_nodes)
        # if self.error:
        #     down_nodes = nodes
        #     target_own_unions = up_nodes[self.target_node]['own_unions'] \
        #         if 'own_unions' in up_nodes[self.target_node] else []
        # else:
        if self.target_node not in up_nodes:
            up_nodes[self.target_node] = nodes
        down_nodes = self._downstream_recursive([self.target_node], nodes)
        target_own_unions = down_nodes[self.target_node]['own_unions'] \
            if 'own_unions' in down_nodes[self.target_node] else []
        target_parent_union = up_nodes[self.target_node]['parent_union'] \
            if 'parent_union' in up_nodes[self.target_node] else ''


        # 分別找upstream/downstream
        # up_nodes = self._upstream_recursive(self.roots, root_nodes)
        # if self.error:
        #     down_nodes = nodes
        # else:
        #     down_nodes = self._downstream_recursive([self.target_node], nodes)
        # target_own_unions = down_nodes[self.target_node]['own_unions'] \
        #     if 'own_unions' in down_nodes[self.target_node] else []
        # target_parent_union = up_nodes[self.target_node]['parent_union'] \
        #     if 'parent_union' in up_nodes[self.target_node] else ''

        merge_nodes.update(up_nodes)
        merge_nodes.update(down_nodes)

        merge_nodes[self.target_node] = {
            'id': self.target_node,
            'own_unions': target_own_unions
            }
        if target_parent_union:
            merge_nodes[self.target_node]['parent_union'] = target_parent_union

        return merge_nodes


class Links():
    def __init__(self, dep_groups, nodes, records):
        self.dep_groups = dep_groups
        self.records = records
        self.nodes = nodes
        self.data = self._gen_links()

    def _for_downstream(self, links):
        """
        因為downstream的部分
        會出現「多個unions對應到一個node」，但是限制條件是「一個node只能有一個parent」
        
        以下方為例
        downstream_node | own_unions | downstream       
        110603          | [2U1]      | ['03','05','18'] 
        110604          | [3U1]      | ['07','05','18'] 

        Node的資訊：node'05'的parent是2U1
            所以links會有一筆資料是['2U1', '05']
        我需要另外將['3U1', '05']加上去

        解法：在所有downstream_nodes中，
            只要該node擁有own_unions並且有downstream，就加入links，最後再掉重複。
        """
        downstream_links = []
        target_downstream = self.dep_groups['downstream']
        for dep_id in target_downstream:
            own_unions = self.nodes[dep_id]['own_unions']
            downstream = self.records[dep_id]['downstream']
            if own_unions and downstream:
                own_union = own_unions[0]
                downstream_links += [[own_union, i] for i in downstream]

        links += downstream_links

        links = [list(x) for x in set(tuple(x) for x in links)]
        return links

    def _gen_base(self):
        """
        透過nodes來連線
        id -> own_unions
        parent_union -> id
        """
        links = []

        for node_id, node in self.nodes.items():
            # id -> own_unions
            for union in node['own_unions']:
                links.append([node_id, union])

            # parent_union -> id
            if 'parent_union' in node:
                links.append([node['parent_union'], node_id])
        return links

    def _gen_links(self):
        """
        [
            [id123, U123],
            [U123, id456]
        ]
        """
        links = self._gen_base()
        links = self._for_downstream(links)
        return links


class Unions():
    def __init__(self, dep_groups, nodes, records):
        self.records = records
        self.dep_groups = dep_groups
        self.nodes = nodes
        self.union_ids = self._get_union_ids()
        self.data = self._gen_unions()

    def _gen_base(self):
        unions = dict()
        for i in self.union_ids:
            unions[i] = {'id': i, 'partner': [], 'children': []}
        return unions

    def _set_partner(self, unions):
        for node_id, node in self.nodes.items():
            for own_union in node['own_unions']:
                unions[own_union]['partner'].append(node_id)
        return unions

    def _set_children(self, unions):
        upstream = self.dep_groups['upstream']
        downstream = self.dep_groups['downstream']

        # for downstream
        for dep_id in downstream:
            own_unions = self.nodes[dep_id]['own_unions']
            if len(own_unions) == 0:
                # 表示是下游的最底層
                pass
            elif len(own_unions) == 1:
                union_id = own_unions[0]
                for i in self.records[dep_id]['downstream']:
                    unions[union_id]['children'].append(i)
            else:
                raise Exception('downstream has more than 1 own_unions')

        # for upstream
        upstream_nodes = {
                node_id: node \
                    for node_id, node in self.nodes.items() \
                        if node_id in upstream
            }
        for node_id, node in upstream_nodes.items():
            if 'parent_union' in node:
                unions[node['parent_union']]['children'].append(node_id)
        return unions

    def _get_union_ids(self):
        ids = []
        for node in self.nodes.values():
            ids += node['own_unions']
        return set(ids)

    def _gen_unions(self):
        """
        {
            "1U5": {
                "children": [
                    "id1475"
                ],
                "id": "1U5",
                "partner": [
                    "id87871475"
                ]
            }
        }
        """
        unions = self._gen_base()
        unions = self._set_partner(unions)
        unions = self._set_children(unions)

        return unions


class DAG:
    """Generate Data For DAG Diagram"""
    def __init__(self, dep_groups, df_deps, target_node):
        self.dep_groups = dep_groups
        self.df_deps = df_deps
        self.records = self._get_records()
        self.target_node = target_node
        self.roots = self._get_roots()
        self.nodes = Nodes(dep_groups=self.dep_groups, target_node=self.target_node, records=self.records, roots=self.roots)
        self.result = True if self.nodes.error == None else False
        self.error = self.nodes.error
        
        # for cycle dag
        groups = {
            'upstream': dep_groups['upstream'].intersection(set(self.nodes.data.keys())),
            'downstream': dep_groups['downstream'].intersection(set(self.nodes.data.keys()))
        }
        
        self.unions = Unions(dep_groups=groups, nodes=self.nodes.data, records=self.records)
        self.links = Links(dep_groups=groups, nodes=self.nodes.data, records=self.records)
        self.data = self._get_data()

    def _get_records(self):
        return {record['id']: record \
                for record in self.df_deps.to_dict('records')}

    def _get_data(self):
        is_success = False if self.nodes.error else True
        data = {
            'result': is_success,
            'data': {
                'start': self.target_node,
                'nodes': self.nodes.data,
                'unions': self.unions.data,
                'links': self.links.data
            }
        }
        if not is_success:
            data['error'] = self.nodes.error
            
        return data

    def _get_roots(self):
        """找出不存在於任何一個node_id的downstream，就是root"""
        ids = set(self.df_deps['id'].to_list())
        downstream = []
        for i in self.df_deps['downstream'].to_list():
            downstream += i

        root = ids - set(downstream)
        return list(root)
        
