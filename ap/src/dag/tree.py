"""Tree Class

This module defines different Tree.
1.AutosysTree: Autosys dependency
2.SjbTree: Sjb dependency
3.TableTree: Table dependency
4.FlowTree: Cjb dependency
5.DiscoveryTree: Discovery Page dependency

"""
from .base import SingleTree, ScopeTree, MultiTree
from src.db import jinjasql_to_df


class AutosysTree(SingleTree):
    """Cjb and Cjb Dependency"""
    """
    ex: ETL_BDS_SAS_Cjb_BDS_DDS_D_RCP
    """
    info = ['id', 'status']
    tooltip = ['cjb']
    def __init__(self, insert_job_id):
        self.insert_job_id = insert_job_id
        self.df = jinjasql_to_df('lineage/autosys_tree.sql')[0]
        super().__init__(insert_job_id, self.df)


class TableTree(SingleTree):
    """Table and Table Dependency"""
    info = ['name', 'status']
    tooltip = ['cname', 'description', 'data_date']
    def __init__(self, table_id):
        self.table_id = table_id
        self.df = jinjasql_to_df('lineage/table_tree.sql', {'id': table_id})[0]
        super().__init__(table_id, self.df)


class FlowTree(ScopeTree):
    """Sjb and Sjb Dependency in a specific Cjb"""
    info = ['job_name', 'status']
    tooltip = ['template', 'data_date', 'datetime']
    def __init__(self, flow_id):
        self.flow_id = flow_id
        self.df = jinjasql_to_df('lineage/flow_tree.sql', {'flow_id': flow_id})[0]
        super().__init__(flow_id, self.df)


class AutosysDiscoveryTree(MultiTree):
    """Multi-Table Dependency
    Attributes:
        ids (list): Multi-IDs
        df (pd.DataFrame): df.columns = ['id', 'upstream', 'downstream']
    """
    info = ['id', 'status']
    tooltip = ['cjb', 'data_date', 'datetime']
    
    def __init__(self, ids:list, df):
        super().__init__(ids, df)


class TableDiscoveryTree(MultiTree):
    """Multi-Table Dependency
    Attributes:
        ids (list): Multi-IDs
        df (pd.DataFrame): df.columns = ['id', 'upstream', 'downstream']
    """
    info = ['name', 'status']
    tooltip = ['cname', 'description', 'data_date']
    def __init__(self, ids:list, df):
        super().__init__(ids, df)

