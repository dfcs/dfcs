"""Tree Base Class

This module defines different styles of Tree.
1.ScopeTree: 以某個虛擬節點展開下游
2.SingleTree: 以某個節點為中心展開
3.MultiTree: 以多個節點為中心展開

"""
from abc import ABC, abstractmethod
from .dag import DAG


class Tree(ABC):
    df = None
    info = list()
    tooltip = list()

    @abstractmethod
    def generate(self) -> dict:
        pass

    def set_node_info(self, nodes):
        for key, val in nodes.items():
            info_data = list()
            tooltip_data = dict()
            record = self.df[self.df['id'] == key]
            if record.shape[0] != 0:
                for i in self.info:
                    info_data.append(record[i].values[0])
                for t in self.tooltip:
                    tooltip_data[t] = record[t].values[0]
            else:
                info_data = [key, '']
            val.update({'info': info_data, 'tooltip': tooltip_data})


class ScopeTree(Tree):
    def __init__(self, scope_name, df):
        self.scope_name = scope_name
        self.df = df  
        self.df_deps = self._df_deps(self.df)
        self.roots = self._get_roots()
        self._add_virtual_node() 

    def _df_deps(self, df):
        """將每筆資料的upstream, downstream都只留下屬於該scope_name的資料"""
        ids = df['id'].to_list()

        def update_stream(stream: list):
            updated_stream = list(set(ids) & set(stream))
            return updated_stream

        df['upstream'] = df['upstream'].apply(update_stream)
        df['downstream'] = df['downstream'].apply(update_stream)

        return df

    def _get_roots(self):
        """找出不存在於任何一個node的downstream，就是root"""
        ids = set(self.df_deps['id'].to_list())
        downstream = []
        for i in self.df_deps['downstream'].to_list():
            downstream += i

        root = ids - set(downstream)
        return list(root)

    def _add_virtual_node(self):
        new_row = {'id': self.scope_name,
                   'upstream': [], 'downstream': self.roots}
        self.df_deps = self.df_deps.append(new_row, ignore_index=True)

    def generate(self):
        dep_groups = {
            'upstream': set(),
            'downstream': set(self.df_deps['id']) - set([self.scope_name])
        }
        data = DAG(
            dep_groups=dep_groups,
            df_deps=self.df_deps,
            target_node=self.scope_name).data
        self.set_node_info(data['data']['nodes'])
        return data


class SingleTree(Tree):
    def __init__(self, id, df):
        self.id = id
        self.df = df
        self.dep_ids, self.dep_groups = self._get_dep_ids()
        self.df_deps = self._get_df_deps()

    def _get_df_deps(self):
        df = self.df[self.df['id'].isin(self.dep_ids)]

        def update_stream(stream: list):
            updated_stream = list(self.dep_ids & set(stream))
            return updated_stream

        df['upstream'] = df['upstream'].apply(update_stream)
        df['downstream'] = df['downstream'].apply(update_stream)
        return df

    def _get_stream_deps(self, data):
        ids = set()
        dep_sets = set()

        _ids = [self.id]

        while _ids:
            for id in _ids:
                dep_sets = dep_sets.union(set(data[id]))
                ids |= set({id})
            _ids = dep_sets - ids
        return dep_sets

    def _get_dep_ids(self):
        data = self.df.to_dict('records')
        upstream_data = {record['id']: record['upstream'] for record in data}
        downstream_data = {record['id']: record['downstream']
                           for record in data}

        upstream_deps = self._get_stream_deps(data=upstream_data)
        downstream_deps = self._get_stream_deps(data=downstream_data)

        dep_ids = upstream_deps.union(downstream_deps)
        dep_ids.add(self.id)

        dep_groups = {
            'upstream': upstream_deps,
            'downstream': downstream_deps
        }
        return dep_ids, dep_groups

    def generate(self):
        data = DAG(
            dep_groups=self.dep_groups,
            df_deps=self.df_deps,
            target_node=self.id).data

        self.set_node_info(data['data']['nodes'])
        return data


class MultiTree(Tree):
    def __init__(self, ids, df):
        self.ids = ids
        self.df = df
        # self._filter_no_autosys()  # 多做這個判斷是因為有案例是「有cjb，但autosys沒有排程」
        self.dep_ids, self.dep_groups = self._get_dep_ids()
        self.df_deps = self._get_df_deps()

    def _filter_no_autosys(self):
        # autosys的IDs
        as_ids = self.df['id'].to_list()

        for i in self.ids:
            if i not in as_ids:
                self.ids.remove(i)

    def _get_stream_deps(self, data):
        ids = set()
        dep_sets = set()

        _ids = self.ids

        while _ids:
            for id in _ids:
                # 多做這個判斷是因為有案例是「有cjb，但autosys沒有排程」
                if id in data:
                    dep_sets = dep_sets.union(set(data[id]))
                    ids |= set({id})
                else:
                    dep_sets.remove(id)

                # dep_sets = dep_sets.union(set(data[id]))
                ids |= set({id})
            _ids = dep_sets - ids
        return dep_sets

    def _get_dep_ids(self):
        data = self.df.to_dict('records')
        upstream_data = {record['id']: record['upstream'] for record in data}
        downstream_data = {record['id']: record['downstream']
                           for record in data}

        # 找出上下游所有的相依dep_ids，一路追到底
        upstream_deps = self._get_stream_deps(data=upstream_data)
        downstream_deps = self._get_stream_deps(data=downstream_data)

        dep_ids = upstream_deps.union(downstream_deps).union(self.ids)

        dep_groups = {
            'upstream': upstream_deps,
            'downstream': downstream_deps
        }
        return dep_ids, dep_groups

    def _get_df_deps(self):
        df = self.df[self.df['id'].isin(self.dep_ids)]

        def update_stream(stream: list):
            updated_stream = list(self.dep_ids & set(stream))
            return updated_stream

        # remove up/down stream not in self.dep_ids，讓上下游只剩下跟target_id相關的
        df['upstream'] = df['upstream'].apply(update_stream)
        df['downstream'] = df['downstream'].apply(update_stream)
        return df

    def _create_downstream_dag(self):
        # add virtual_upstream node
        virtual_node = 'virtual_upstream'
        new_row = {'id': virtual_node, 'upstream': [], 'downstream': self.ids}
        df = self.df_deps.copy().append(new_row, ignore_index=True)
        dep_groups = {
            'upstream': set(),
            'downstream': self.dep_groups['downstream'].union(self.ids)
        }
        ids = dep_groups['downstream'].union([virtual_node])
        df = df[df['id'].isin(ids)]

        def update_stream(stream: list):
            updated_stream = list(dep_groups['downstream'] & set(stream))
            return updated_stream

        # remove up/down stream not in self.dep_ids，讓上下游只剩下跟target_id相關的
        df['upstream'] = df['upstream'].apply(update_stream)
        df['downstream'] = df['downstream'].apply(update_stream)

        dag = DAG(
            dep_groups=dep_groups,
            df_deps=df,
            target_node=virtual_node)
        return dag.data

    def _create_upstream_dag(self):
        # add virtual_downstream node
        virtual_node = 'virtual_downstream'

        new_row = {'id': virtual_node, 'upstream': self.ids, 'downstream': []}
        df = self.df_deps.copy().append(new_row, ignore_index=True)

        # update root nodes, add virtual node in upstream.
        # df.index = df['id']
        # for i in self.ids:
        #     df.at[i, 'downstream'] = [virtual_node]

        dep_groups = {
            'upstream': self.dep_groups['upstream'].union(self.ids),
            'downstream': set()
        }
        ids = dep_groups['upstream'].union([virtual_node])
        df = df[df['id'].isin(ids)]

        def update_stream(stream: list):
            updated_stream = list(dep_groups['upstream'] & set(stream))
            return updated_stream

        # remove up/down stream not in self.dep_ids，讓上下游只剩下跟target_id相關的
        df['upstream'] = df['upstream'].apply(update_stream)
        df['downstream'] = df['downstream'].apply(update_stream)

        dag = DAG(
            dep_groups=dep_groups,
            df_deps=df,
            target_node=virtual_node)
        return dag.data

    def _merge(self, downstream_dag, upstream_dag):
        downstream_data = downstream_dag['data']
        upstream_data = upstream_dag['data']
        for i in self.ids:
            # 這個判斷是為了因應DAG循環問題
            if i in upstream_data['nodes'] and 'parent_union' in upstream_data['nodes'][i]:
                downstream_data['nodes'][i]['parent_union'] = upstream_data['nodes'][i]['parent_union']
                del upstream_data['nodes'][i]
            else:
                # there has no parent_union
                pass
        merge_nodes = {**downstream_data['nodes'], **upstream_data['nodes']}
        # own_unions需要merge而不是取代
        for key, val in downstream_data['nodes'].items():
            if key in upstream_data['nodes']:
                merge_nodes[key]['own_unions'] = val['own_unions'] + upstream_data['nodes'][key]['own_unions']

                
        merge_unions = {**downstream_data['unions'], **upstream_data['unions']}
        merge_links = downstream_data['links'] + upstream_data['links']

        is_success = downstream_dag['result'] and upstream_dag['result']
        data = {
            'result': is_success,
            'data': {
                'start': downstream_data['start'],
                'nodes': merge_nodes,
                'unions': merge_unions,
                'links': merge_links
            }
        }

        return data

    def _hidden_virtual_node(self, dag_data):
        """Virtual Node and its own_unions add hidden attribute"""
        id = dag_data['start']
        dag_data['nodes'][id]['hidden'] = True
        dag_data['unions'][dag_data['nodes'][id]
                           ['own_unions'][0]]['hidden'] = True
        return dag_data

    def generate(self):
        downstream_dag = self._create_downstream_dag()
        upstream_dag = self._create_upstream_dag()
        # result =
        data = self._merge(downstream_dag, upstream_dag)
        data['data'] = self._hidden_virtual_node(data['data'])
        self.set_node_info(data['data']['nodes'])
        return data
