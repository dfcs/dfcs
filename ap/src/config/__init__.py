import os
from configparser import ConfigParser
import logging
import json
from src.util.encrypt import decrypt_message
LOGGER = logging.getLogger('dfap')


DEFAULT_CFG = 'src/config/config.ini'

def get_env_config(filename=DEFAULT_CFG):
    config = ConfigParser()
    config.read(filename)
    return config


ENV_CONFIG = get_env_config()

def configs(filename, sections=None, wildcard=True):
    parser = ConfigParser()
    parser.read(filename)
    # get section, default to postgresql
    data = {}
    tar_sections = []
    src_sections = parser.sections()
    if sections is None:
        tar_sections = src_sections
    else:
        tar_sections = [x for x in sections if x in src_sections]

    LOGGER.info(f'I will load the config sections:{tar_sections}')
    for section in tar_sections:
        params = parser.items(section)
        section_data = {key.upper(): val for key, val in parser.items(section)}
        data[section] = section_data
    
    return data


def get_db_uri(db_info: dict):
    db_info['DB_PASSWORD'] = decrypt_message(db_info['DB_PASSWORD'])
    db_uri = \
        'postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_DATABASE}' \
        .format(**db_info)

    return db_uri

def get_config_data(config_file=None, app=None):
    use_config = None
    if config_file:
        use_config = config_file
    elif os.environ.get('DFAP_CONFIG'):
        use_config = os.environ.get('DFAP_CONFIG')
    else:
        use_config = DEFAULT_CFG

    data = configs(sections=['DFCS','DFCS_SVR','DFAP', 'DFCS_DB'], filename=use_config)

    data['SQLALCHEMY_DATABASE_URI'] = get_db_uri(data['DFCS_DB'])

    if app:
        app.config.from_mapping(data)
    else:
        pass

    return data