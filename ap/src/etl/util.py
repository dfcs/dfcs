"""Common util"""
from src.db import jinjasql_execute


def get_syscode_data():
    """
    {
        'type_id': [{
            'code_id': 'code_data_json'
        }]
    }
    """
    rs_list = jinjasql_execute(template='get_syscode_data.sql', template_home='SQL_DIR')[0]
    data = {i['type_id']: i['data_set'] for i in rs_list}

    return data