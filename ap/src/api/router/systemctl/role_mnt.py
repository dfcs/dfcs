"""
Author: Hank Su
Description: role_mnt
"""


from flask import Blueprint, jsonify, request
import logging
import pandas as pd
from src.api.react_admin_util import ReactAdminListReq, make_react_admin_response
from src.api.util import get_item_info
from src.security.decorator import user_has
from src.db import jinjasql_to_df
import json
from src.security.decorator import user_has
from src.etl.util import get_syscode_data

bp = Blueprint('role_mnt',__name__)
LOGGER = logging.getLogger('dfap')


@bp.route('/role_mnt',methods=['GET'])
@user_has(['systemctl/role_mnt/enable'])
def get_list():
    ra_q = ReactAdminListReq(request)
    cnt, df = ra_q.list_sql_query('systemctl/role_mnt_query.sql')
    return make_react_admin_response(body=df.to_json(orient='records'), httpcode=200, count=cnt)

@bp.route('/role_mnt',methods=['POST'])
@user_has(['systemctl/role_mnt/enable'])
def create():
    r_data = request.json
    LOGGER.info(r_data)

    menu_privileges = get_syscode_data()['DFAP_CFG_PRIVILEGE']['default']
    import json

    jd = jinjasql_to_df('systemctl/role_mnt_create.sql', {'name' : r_data['name'],'menu_privileges' :json.dumps(menu_privileges) })
    return make_react_admin_response(jsonify({}))

@bp.route('/role_mnt/<id>',methods=['PUT'])
@user_has(['systemctl/role_mnt/enable'])
def update(id):
    r_data = request.json
    LOGGER.info(r_data)
    jd = jinjasql_to_df('systemctl/role_mnt_update.sql', {'id':id, 'menu_privileges':json.dumps(r_data['menu_privileges'])})
    return make_react_admin_response(jsonify({}))

@bp.route('/role_mnt/<id>',methods=['DELETE'])
@user_has(['systemctl/role_mnt/enable'])
def delete(id):
    try:
        jd = jinjasql_to_df('systemctl/role_mnt_delete.sql', {'id': id})
    except Exception as e :
        raise Exception("delete error")

    return make_react_admin_response(jsonify({}))