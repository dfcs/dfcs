"""
Author: Hank Su
Description: account_mnt
"""

from flask import Blueprint, jsonify, request
import logging
import pandas as pd
from src.api.react_admin_util import ReactAdminListReq, make_react_admin_response
from src.api.util import get_item_info
from src.security.decorator import user_has
from src.db import jinjasql_to_df
from src.api.util import get_item_info
from src.security.decorator import user_has

bp = Blueprint('account_mnt',__name__)
LOGGER = logging.getLogger('dfap')


@bp.route('/account_mnt',methods=['GET'])
@user_has(['systemctl/account_mnt/enable'])
def get_list():
    ra_q = ReactAdminListReq(request)
    cnt, df = ra_q.list_sql_query('systemctl/account_mnt_query.sql')
    return make_react_admin_response(body=df.to_json(orient='records'), httpcode=200, count=cnt)

@bp.route('/account_mnt',methods=['POST'])
@user_has(['systemctl/account_mnt/enable'])
def create():
    r_data = request.json
    LOGGER.info(r_data)

    jd = jinjasql_to_df('systemctl/account_mnt_create.sql', r_data)
    return make_react_admin_response(jsonify({}))

@bp.route('/account_mnt/<id>',methods=['PUT'])
@user_has(['systemctl/account_mnt/enable'])
def update(id):
    r_data = request.json
    LOGGER.info(r_data)
    
    data = get_item_info('systemctl/account_mnt_update.sql', {'id':id, 'role_id' : r_data['role_id']})
    return make_react_admin_response(jsonify(data))

@bp.route('/account_mnt/<id>',methods=['DELETE'])
@user_has(['systemctl/account_mnt/enable'])
def delete(id):
    jd = jinjasql_to_df('systemctl/account_mnt_delete.sql', {'id': id})
    return make_react_admin_response(jsonify({}))