"""
Author: Howard Ye
Description: authentication
"""
from flask import Blueprint, jsonify, request
from flask_security.utils import login_user, logout_user
from src.security import User
from src.security.utils import merge_privileges
from src.error_code import DFAPLoginException
from ldap3 import Server, Connection, ALL
import logging
from src.api.react_admin_util import make_react_admin_response
from src.security.vulnerability import verify_pwd

bp = Blueprint('auth',__name__)
LOGGER = logging.getLogger('dfap')


@bp.route('/login',methods=['POST'])
def login():
    userInfo = request.json
    user = User.query.filter_by(username=userInfo.get("username")).first()
    
    if user==None:
        raise DFAPLoginException("User is not exists in our system.")
    elif user.username == 'dfcsadmin':
        if verify_pwd(userInfo['password'], user.password):
            pass
        else:
            raise DFAPLoginException("wrong password")
    else:
        try:
            host = Server('ldap://ldap.com:389', get_info=ALL)
            conn = Connection(host, user=f"ABC\\{userInfo['username']}", password=userInfo['password'], check_names=True, lazy=False, raise_exceptions=True)
            conn.open()
            conn.bind()
        except:
            raise DFAPLoginException("AD authentication failed")
    
    login_user(user, remember=False)
    data = merge_privileges([x.menu_privileges for x in user.roles])
    return make_react_admin_response(jsonify(data))

@bp.route('/logout',methods=['GET'])
def logout():
    logout_user()
    return make_react_admin_response(jsonify({"msg":"ok"}))
