"""
Author: Hank Su
Description: dashboard
"""

from flask import Blueprint, jsonify, request
import logging
import pandas as pd
import copy
from datetime import date, datetime
from src.db import jinjasql_to_df
from src.api.react_admin_util import make_react_admin_response
from src.etl.util import get_syscode_data


bp = Blueprint('dashboard',__name__)
LOGGER = logging.getLogger('dfap')


DEFAULT_STAT = {
    '00:00~01:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '01:00~02:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '02:00~03:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '03:00~04:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '04:00~05:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '05:00~06:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '06:00~07:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '07:00~08:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '08:00~09:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '09:00~10:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '10:00~11:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '11:00~12:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '12:00~13:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '13:00~14:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '14:00~15:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '15:00~16:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '16:00~17:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '17:00~18:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '18:00~19:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '19:00~20:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '20:00~21:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '21:00~22:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '22:00~23:00': {'success': 0,'error': 0,'running': 0,'waiting': 0},
    '23:00~24:00': {'success': 0,'error': 0,'running': 0,'waiting': 0}
}

def get_job_status(df_job, df_job_status):
    data_job = {i['name']: {'all': i['all']} for i in df_job.to_dict('records')}

    data_status = dict()
    for i in df_job_status.to_dict('records'):
        if i['name'] not in data_status:
            data_status[i['name']] = {i['status']: i['count']}
        else:
            data_status[i['name']][i['status']] = i['count']

    data_stat = copy.copy(data_job)
    for i in data_job:
        if i in data_status:
            data_stat[i].update(data_status[i])

    for i in data_stat:
        init_data = {
            'all': 0,
            'error': 0,
            'success': 0,
            'waiting': 0
        }
        init_data.update(data_stat[i])
        data_stat[i] = init_data
        data_stat[i]['name'] = i

    for key, val in data_stat.items():
        data_stat[key]['waiting'] = data_stat[key]['all'] - data_stat[key]['error'] - data_stat[key]['success']
    data_stat = list(data_stat.values())

    return data_stat

def hourly_data_by_tag(tag, real_stat=dict()):
    stat = copy.deepcopy(DEFAULT_STAT)
    for key, val in real_stat.items():
        stat[key].update(val)

    data = list()
    for t, status_dict in stat.items():
        status_dict.update({'name': t})
        data.append(status_dict)

    return data

def get_job_status_hourly(data_tag, records):
    all_real_data = dict()
    for i in records:
        item = {i['sche_range']: {i['status']: i['count']}}
        if i['name'] in all_real_data:
            if i['sche_range'] in all_real_data[i['name']]:
                all_real_data[i['name']][i['sche_range']].update({i['status']: i['count']})
            else:
                all_real_data[i['name']].update(item)
        else:
            all_real_data[i['name']] = item

    data = dict()
    for tag, real_stat in all_real_data.items():
        hourly_data = hourly_data_by_tag(tag, real_stat)

        data[tag] = hourly_data

    others = set(data_tag) - set(all_real_data.keys())
    for tag in others:
        hourly_data = hourly_data_by_tag(tag)
        data[tag] = hourly_data
    return data

@bp.route('/dashboard/monitor',methods=['GET'])
def monitor():
    dt = request.args['dt'] if 'dt' in request.args \
        else date.today().strftime("%Y-%m-%d")

    data_tag_data = get_syscode_data()['DFEG_JOB_STATISTIC']['data_tag']
    
    search_keys = list()
    for i in data_tag_data:
        search_keys.append(list(i.keys())[0])
    mapping_key = dict()
    for i in data_tag_data:
        mapping_key.update(i)
    mapping_key['ALL'] = 'ALL'

    target_tag = search_keys[:3]

    df_job, df_job_status, df_job_status_hourly, df_alarm_log = \
        jinjasql_to_df('dashboard/monitor.sql', {'dt': dt, 'target_tag': target_tag, 'data_tag': search_keys})

    job_status = get_job_status(df_job, df_job_status)
    for i in job_status:
        i['search_key'] = i['name']
        i['name'] = mapping_key[i['name']]

    job_status_hourly = get_job_status_hourly(
        search_keys, df_job_status_hourly.to_dict('records'))
    for i in search_keys:
        job_status_hourly[mapping_key[i]] = job_status_hourly.pop(i)

    df_alarm_log['id'] = df_alarm_log.index
    alert = df_alarm_log.to_dict('records')

    data = {
        "job_status": job_status,
        "job_status_hourly": job_status_hourly,
        "alert": alert
    }
    return make_react_admin_response(jsonify(data))

@bp.route('/dashboard/res_usage',methods=['GET'])
def res_usage():
    df = jinjasql_to_df('dashboard/res_usage.sql')[0]
    df['threshold'] = df['threshold'].astype(float)
    data = df.to_dict('records')

    return make_react_admin_response(jsonify(data))