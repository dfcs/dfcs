"""
Author: Hank Su
Description: data lineage
"""

from flask import current_app, Blueprint, request, jsonify
import logging
import requests
from src.dag.tree import (
    FlowTree, TableTree, AutosysTree,
    AutosysDiscoveryTree, TableDiscoveryTree
)
from src.api.util import get_item_info
from src.db import jinjasql_to_df
from src.api.react_admin_util import make_react_admin_response

bp = Blueprint('dl', __name__)
LOGGER = logging.getLogger('dfap')


@bp.route('/dl/single/<type>', methods=['GET'])
def single(type):
    id = request.args['id']
    if id == 'null':
        return {}
    if type == 'table':
        tree = TableTree(table_id=id)
    elif type == 'cjb':
        tree = AutosysTree(insert_job_id=id)
    data = tree.generate()
    return make_react_admin_response(jsonify(data))

@bp.route('/dl/multi/<type>', methods=['GET'])
def multi(type):
    ids = request.args.getlist('ids')

    if type == 'table':
        df = jinjasql_to_df('lineage/table_tree.sql')[0]
        tree = TableDiscoveryTree(ids, df)
    elif type == 'cjb':
        df = jinjasql_to_df('lineage/discover_cjb_tree.sql')[0]
        tree = AutosysDiscoveryTree(ids, df)
    data = tree.generate()
    return make_react_admin_response(jsonify(data))

@bp.route('/dl/scope/job', methods=['GET'])
def scope():
    id = request.args['id']
    cjb_id = get_item_info('common/get_cjb_by_sjb.sql', {'id': id})['flow_id']

    tree = FlowTree(cjb_id)
    data = tree.generate()

    data['data']['nodes'][id]['target'] = True

    return make_react_admin_response(jsonify(data))


@bp.route('/dl/sql', methods=['POST'])
def dl_sql():
    data = {}

    DFDL_SVR_GRAPH = current_app.config['DFCS_SVR']['DFDL_SVR_GRAPH']
    body = requests.post(DFDL_SVR_GRAPH, data=data).json()
    return make_react_admin_response(jsonify(body))


@bp.route('/dl/license', methods=['GET'])
def license():
    data = {}
    return make_react_admin_response(jsonify(data))
