"""
Author: Hank Su
Description: Autocomplete API
"""
from flask import Blueprint, current_app, request

from src.api.react_admin_util import ReactAdminListReq, make_react_admin_response
import logging
LOGGER = logging.getLogger('dfap')
bp = Blueprint('ac', __name__)


@bp.route('/ac/<string:autocomplete>', methods=['GET'])
def ac(autocomplete):
    ra_q = ReactAdminListReq(request)
    template = f'common/autocomplete/{autocomplete}.sql'
    len, df_data = ra_q.ac_sql_query(template, autocomplete)

    return make_react_admin_response(body=df_data.to_json(orient='records'), httpcode=200, count=len)