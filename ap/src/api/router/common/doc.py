"""
Author: Hank Su
Description: Swagger UI Router
"""
from flask import Blueprint, render_template
import logging
LOGGER = logging.getLogger('dfap')

bp = Blueprint('doc',__name__)

@bp.route('/docs')
def get_docs():
    return render_template('swaggerui.html')
