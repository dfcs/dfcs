"""
Author: Hank Su
Description: data_usage_logs
"""

from flask import Blueprint, jsonify, request
import logging
import pandas as pd
from src.api.react_admin_util import ReactAdminListReq, make_react_admin_response
from src.api.util import get_item_info
from src.config import ENV_CONFIG
from src.security.decorator import user_has

bp = Blueprint('data_usage_logs', __name__)
LOGGER = logging.getLogger('dfap')


@bp.route('/data_usage_logs', methods=['GET'])
@user_has(['assets/data_usage_logs/enable'])
def data_usage_logs():
    ra_q = ReactAdminListReq(request)
    len, df = ra_q.list_sql_query('assets/data_usage_logs.sql')
    return make_react_admin_response(body=df.to_json(orient='records'), httpcode=200, count=len)
