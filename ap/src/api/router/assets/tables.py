"""
Author: Hank Su
Description: tables
"""

from flask import Blueprint, jsonify, request
import logging
import pandas as pd
from src.api.react_admin_util import ReactAdminListReq, make_react_admin_response
from src.api.util import get_item_info
from src.db import jinjasql_to_df
from src.security.decorator import user_has

bp = Blueprint('tables', __name__)
LOGGER = logging.getLogger('dfap')


@bp.route('/tables', methods=['GET'])
@user_has(['assets/tables/enable'])
def tables():
    ra_q = ReactAdminListReq(request)
    if 'show_tmp_bool' not in request.args:
        ra_q.filters.append({'key': 'show_tmp_bool', 'value': 'false'})
    cnt, df = ra_q.list_sql_query('assets/tables/tables.sql')
    return make_react_admin_response(body=df.to_json(orient='records'), httpcode=200, count=cnt)

@bp.route('/tables/<id>/table_info', methods=['GET'])
@user_has(['assets/tables/enable'])
def table_info(id):
    data = get_item_info('assets/tables/table_info.sql', {'id': id})
    data['order'] = [
        'catalog','db','system','table_name','table_cname','table_description_br','table_data_type',
        'table_data_frequence','table_process','dependent_col_arr','source_table_arr',
        'table_tags_json_arr','output_flag_arr','upd_date_dt','data_format','it_team',
        'remove_date_dt','create_date_dt','modify_date_dt','tstat_row_count',
        'tstat_statistic_date_dt','tstat_size','platform']
    return make_react_admin_response(jsonify(data))

@bp.route('/tables/<id>/columns', methods=['GET'])
@user_has(['assets/tables/enable'])
def columns(id):
    ra_q = ReactAdminListReq(request)
    len, df = ra_q.list_sql_query('assets/tables/columns.sql', {'id': id})
    return make_react_admin_response(body=df.to_json(orient='records'), httpcode=200, count=len)

@bp.route('/tables/<id>/columns/<cid>', methods=['GET'])
@user_has(['assets/tables/enable'])
def column_details(id, cid):
    data = get_item_info('assets/tables/column_details.sql', {'id': cid})
    data['order'] = [
        'table_name', 'col_ordinary_int', 'col_name', 'col_data_type_str', 
        'col_pk_bool', 'col_nullable_bool', 'col_cname', 'col_description',
        'remark', 'personal_info', 'upd_date', 'col_update_key_bool'
    ]
    return make_react_admin_response(jsonify(data))

@bp.route('/tables/<id>/job_info', methods=['GET'])
@user_has(['assets/tables/enable'])
def job_info(id):
    df = jinjasql_to_df('assets/tables/job_info.sql', {'id': id})[0]
    records = df.to_dict(orient='records')
    data = [i['all_params'] for i in records]
    return make_react_admin_response(jsonify(data))

@bp.route('/tables/<id>/cjb_info', methods=['GET'])
@user_has(['assets/tables/enable'])
def cjb_info(id):
    ra_q = ReactAdminListReq(request)
    cnt, df = ra_q.list_sql_query('assets/tables/cjb_info.sql', {'id': id})
    return make_react_admin_response(body=df.to_json(orient='records'), httpcode=200, count=cnt)

@bp.route('/tables/<id>/process_log', methods=['GET'])
@user_has(['assets/tables/enable'])
def process_log(id):
    ra_q = ReactAdminListReq(request)
    len, df = ra_q.list_sql_query('assets/tables/process_log.sql', {'id': id})
    return make_react_admin_response(body=df.to_json(orient='records'), httpcode=200, count=len)

@bp.route('/tables/<id>/process_log/<pid>', methods=['GET'])
@user_has(['assets/tables/enable'])
def process_log_details(id, pid):
    sys_code, tbl_name, datetime = pid.split('@')
    data = get_item_info(
        'assets/tables/process_log_details.sql',
        {
            'sys_code': sys_code,
            'tbl_name': tbl_name,
            'datetime': datetime
        }
    )
    return make_react_admin_response(jsonify(data))
