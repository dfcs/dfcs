"""
Author: Hank Su
Description: data_dep_discover
"""

from flask import Blueprint, jsonify, request
import logging
import pandas as pd
from src.api.react_admin_util import ReactAdminListReq, make_react_admin_response
from src.api.util import get_item_info
from src.config import ENV_CONFIG
from src.security.decorator import user_has

bp = Blueprint('data_dep_discover', __name__)
LOGGER = logging.getLogger('dfap')

@bp.route('/data_dep_discover', methods=['GET'])
@user_has(['assets/data_dep_discover/enable'])
def discover():
    if ('data_tag' in request.args) or ('table_name' in request.args) or ('table_cname' in request.args):
        ra_q = ReactAdminListReq(request)
        fid = ra_q.args['fid']
        template = f'assets/data_dep_discover/data_dep_discover_{fid}.sql'
        len, df = ra_q.list_sql_query(template, query_black_list=['fid'])
        return make_react_admin_response(body=df.to_json(orient='records'), httpcode=200, count=len)
    else:
        return make_react_admin_response(body=jsonify(list()), httpcode=200, count=0)
