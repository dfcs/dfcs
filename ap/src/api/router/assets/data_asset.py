"""
Author: Hank Su
Description: data_asset
"""

from flask import Blueprint, jsonify
from src.db import jinjasql_to_df
import logging
from src.security.decorator import user_has
from src.api.react_admin_util import make_react_admin_response

bp = Blueprint('data_asset', __name__)
LOGGER = logging.getLogger('dfap')


@bp.route('/data_asset', methods=['GET'])
@user_has(['assets/data_asset/enable'])
def data_asset():

    data_by_catalog, data_by_db, data_by_output_flag, db_list_in_dm = \
        jinjasql_to_df('assets/data_asset.sql')

    all_real_data = dict()
    for i in data_by_db.to_dict('records'):
        item = {'summary': {'table_cnt': i['table_cnt'],
                            'record_cnt': i['record_cnt'],
                            'size_cnt': i['size_cnt']},

                }
        all_real_data[i['db']] = item

    for i in data_by_catalog.to_dict('records'):
        if i['db'] in all_real_data:
            if 'child' not in all_real_data[i['db']]:
                all_real_data[i['db']]['child'] = {}
            if i['db'] == 'DM':
                all_real_data[i['db']]['child'][i['ext_catalog']] = \
                    {'summary': {'table_cnt': i['table_cnt'],
                                 'record_cnt': i['record_cnt'],
                                 'size_cnt': i['size_cnt']
                                 }}
            else:
                all_real_data[i['db']]['child'][i['ext_catalog']] = \
                    {'summary': {'table_cnt': i['table_cnt'],
                                 'record_cnt': i['record_cnt'],
                                 'size_cnt': i['size_cnt'],
                                 'db': [i['db']]}}

    for j in db_list_in_dm.to_dict('records'):
        if all_real_data['DM']['child'][j['ext_catalog']] is not None:
            item = all_real_data['DM']['child'][j['ext_catalog']]['summary']
            if 'db' not in item:
                item['db'] = []
                item['db'].append(j['db'])
            else:
                if i['db'] not in item['db']:
                    item['db'].append(j['db'])

    all_real_data['out'] = {'child': {}}
    for i in data_by_output_flag.to_dict('records'):
        all_real_data['out']['child'][i['output_flag']] = i['table_cnt']

    return make_react_admin_response(jsonify(all_real_data))
