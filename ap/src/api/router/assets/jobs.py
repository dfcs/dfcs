"""
Author: Hank Su
Description: jobs
"""

from flask import Blueprint, jsonify, request
import logging
import pandas as pd
from src.api.react_admin_util import ReactAdminListReq, make_react_admin_response
from src.api.util import get_item_info
from src.config import ENV_CONFIG
from src.db import jinjasql_to_df
from src.security.decorator import user_has

bp = Blueprint('jobs', __name__)
LOGGER = logging.getLogger('dfap')


@bp.route('/jobs', methods=['GET'])
@user_has(['assets/jobs/enable'])
def jobs():
    ra_q = ReactAdminListReq(request)
    if 'sche_date' in request.args:
        len, df = ra_q.list_sql_query('assets/jobs/sche_jobs.sql', {'dt': request.args['sche_date'][0:10]})
    else:
        len, df = ra_q.list_sql_query('assets/jobs/jobs.sql')
    return make_react_admin_response(body=df.to_json(orient='records'), httpcode=200, count=len)


@bp.route('/jobs/<id>/job_info', methods=['GET'])
@user_has(['assets/jobs/enable'])
def job_info(id):
    df = jinjasql_to_df('assets/jobs/job_info.sql', {'id': id})[0]
    records = df.to_dict(orient='records')
    data = [i['all_params'] for i in records]
    return make_react_admin_response(jsonify(data))


@bp.route('/jobs/<id>/cjb_info', methods=['GET'])
@user_has(['assets/jobs/enable'])
def cjb_info(id):
    ra_q = ReactAdminListReq(request)
    cnt, df = ra_q.list_sql_query('assets/jobs/cjb_info.sql', {'id': id})
    return make_react_admin_response(body=df.to_json(orient='records'), httpcode=200, count=cnt)


@bp.route('/jobs/<id>/process_log', methods=['GET'])
@user_has(['assets/jobs/enable'])
def process_log(id):
    ra_q = ReactAdminListReq(request)
    len, df = ra_q.list_sql_query('assets/jobs/process_log.sql', {'id': id})
    return make_react_admin_response(body=df.to_json(orient='records'), httpcode=200, count=len)


@bp.route('/jobs/<id>/process_log/<pid>', methods=['GET'])
@user_has(['assets/jobs/enable'])
def process_log_details(id, pid):
    sys_code, tbl_name, datetime = pid.split('@')
    data = get_item_info(
        'assets/jobs/process_log_details.sql',
        {
            'sys_code': sys_code,
            'tbl_name': tbl_name,
            'datetime': datetime
        }
    )
    return make_react_admin_response(jsonify(data))


@bp.route('/jobs/<id>/table_info', methods=['GET'])
@user_has(['assets/jobs/enable'])
def table_info(id):
    data = get_item_info('assets/jobs/table_info.sql', {'id': id})
    return make_react_admin_response(jsonify(data))


@bp.route('/jobs/<id>/columns', methods=['GET'])
@user_has(['assets/jobs/enable'])
def columns(id):
    ra_q = ReactAdminListReq(request)
    len, df = ra_q.list_sql_query('assets/jobs/columns.sql', {'id': id})
    return make_react_admin_response(body=df.to_json(orient='records'), httpcode=200, count=len)


@bp.route('/jobs/<id>/columns/<cid>', methods=['GET'])
@user_has(['assets/jobs/enable'])
def column_details(id, cid):
    data = get_item_info('assets/tables/column_details.sql', {'id': cid})
    data['order'] = [
        'table_name', 'col_ordinary_int', 'col_name', 'col_data_type_str', 
        'col_pk_bool', 'col_nullable_bool', 'col_cname', 'col_description',
        'remark', 'personal_info', 'upd_date', 'col_update_key_bool'
    ]
    return make_react_admin_response(jsonify(data))


@bp.route('/jobs/<id>/sqls', methods=['GET'])
@user_has(['assets/jobs/enable'])
def get_sqls(id):
    res = get_item_info('assets/jobs/get_sqls.sql', {'id': id})
    if res:
        root_dir = ENV_CONFIG['CL']['sql_dir']
        sql_list = eval(res['sqls'])
        sqls_data = list()
        for i in sql_list:
            if i['path'] != '':
                sql_path = root_dir + i['path']
                with open(sql_path, 'r') as f:
                    sql_context = f.read()
                item = {
                    'name': i['name'],
                    'path': i['path'],
                    'sql': sql_context
                }
                sqls_data.append(item)
        data = {
            'job_name': res['job_name'],
            'flow_name': res['flow_name'],
            'sqls': sqls_data
        }

    return make_react_admin_response(jsonify(data))
