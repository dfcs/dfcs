"""
Author: Howard Ye
Description: API Router
"""

import os
import json
from src.security import Role, User
from flask_cors import CORS
from flask_security import Security, SQLAlchemySessionUserDatastore
import logging
from src.config import get_config_data
from flask import Flask, redirect, session
from flask_cors import CORS
from src.db import db
from datetime import timedelta


LOGGER = logging.getLogger('dfap')


def register_route(app):
    # common api
    from .router import auth
    from .router.common import autocomplete
    from .router import lineage
    from . import error_handler
    app.register_blueprint(auth.bp,url_prefix='/api')
    app.register_blueprint(autocomplete.bp,url_prefix='/api')
    app.register_blueprint(lineage.bp,url_prefix='/api')
    app.register_blueprint(error_handler.bp,url_prefix='/api')

    # page api
    from .router import dashboard
    from .router.assets import (
        data_dep_discover, data_asset, tables, jobs, data_usage_logs
    )
    from .router.systemctl import (
        account_mnt, role_mnt
    )
    app.register_blueprint(dashboard.bp,url_prefix='/api')
    app.register_blueprint(data_dep_discover.bp,url_prefix='/api')
    app.register_blueprint(data_asset.bp,url_prefix='/api')
    app.register_blueprint(tables.bp,url_prefix='/api')
    app.register_blueprint(jobs.bp,url_prefix='/api')
    app.register_blueprint(data_usage_logs.bp,url_prefix='/api')
    app.register_blueprint(account_mnt.bp,url_prefix='/api')
    app.register_blueprint(role_mnt.bp,url_prefix='/api')

def register_swagger(app):
    # for swagger UI
    from .router.common import doc
    app.register_blueprint(doc.bp, url_prefix='/api')

    @app.route('/')
    def index():
        return redirect("/api/docs", code=302)

def create_app(test_config=None):
    app = Flask(__name__,instance_relative_config=True)

    # set app.config
    env_config = get_config_data(test_config, app)

    db.init_app(app)
    app.app_context().push()

    register_route(app)
    if env_config['DFAP']['SWAGGER_UI'] == 'true':
        register_swagger(app)
    
    CORS(app, support_credentials=True)

    env = app.jinja_env

    from src.db import db_session, init_db

    # Setup the security
    app.config['SECRET_KEY'] = env_config.get("SECRET_KEY", 'xxxxxxx')
    app.config['SECURITY_PASSWORD_SALT'] = env_config.get("SECURITY_PASSWORD_SALT", 'xxxxxxx')
    app.config['SECURITY_TRACKABLE']=True
    app.config['SESSION_COOKIE_SAMESITE'] = 'Strict'
    user_datastore = SQLAlchemySessionUserDatastore(db_session, User, Role)
    security = Security(app, user_datastore)
    security = app.extensions['security']
    # Setup the DB Prerequisite
    def _check_prerequisite():
        try:
            user = User.query.first()
        except Exception as e:
            db_session.rollback()
            LOGGER.info(f'There is no Database , I will init db')
            init_db()
            db_session.commit()
            from src.security.utils import re_import_user_data
            re_import_user_data()
    app.before_first_request(_check_prerequisite)
    
    @app.before_request
    def make_session_permanent():
        session.permanent = True
        app.permanent_session_lifetime = timedelta(minutes=30)

    return app
