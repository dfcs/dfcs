"""
Author: Howard Ye
Description: I will handle the exception of flask app
"""
import sys
import traceback
from flask import Blueprint, make_response, jsonify
from src.api.react_admin_util import ReactAdminListReq
from src.error_code import DFAPWebException, DFAPDBException,DFAPLoginException
from src.db import db_session
import logging
LOGGER = logging.getLogger("dfap")
bp = Blueprint('errors',__name__)

@bp.app_errorhandler(Exception)
def handle_error_all(error):
    LOGGER.exception(f"unknow Exception, coz {error.__class__.__name__} Exception")
    db_session.rollback()
    message = [str(x) for x in error.args]
    response = {
        'success': False,
        'error': {
            'type': error.__class__.__name__,
            'message': message
        }
    }
    LOGGER.error(f"I got error:{error}")
    traceback.print_tb(error.__traceback__)
    return jsonify(response), 400


@bp.app_errorhandler(DFAPLoginException)
def handle_error_login(error):
    # exec_info = sys.exc_info()
    LOGGER.exception(f"Rollback the DB session, coz {error.__class__.__name__} Exception")
    db_session.rollback()
    message = [str(x) for x in error.args]
    status_code = error.status_code
    success = False
    response = {
        'success': success,
        'error': {
            'type': error.__class__.__name__,
            'message': message,
            'sql': "" if not hasattr(error, 'sql') else error.sql
        }
    }
    return jsonify(response), 403



@bp.app_errorhandler(DFAPWebException)
def handle_error(error):
    # exec_info = sys.exc_info()
    LOGGER.exception(f"Rollback the DB session, coz {error.__class__.__name__} Exception")
    db_session.rollback()
    message = [str(x) for x in error.args]
    status_code = error.status_code
    success = False
    response = {
        'success': success,
        'error': {
            'type': error.__class__.__name__,
            'message': message,
            'sql': "" if not hasattr(error, 'sql') else error.sql
        }
    }
    return jsonify(response), status_code