import logging
from src.db import jinjasql_to_df

LOGGER = logging.getLogger('dfap')

def get_item_info(template, params=dict()):
    df = jinjasql_to_df(template, params=params)[0]
    records = df.to_dict(orient='records')
    if len(records) == 1:
        data = df.to_dict(orient='records')[0]
    elif len(records) == 0:
        data = {}
    else:
        LOGGER.warn('Lens of result records more than one.')
        data = df.to_dict(orient='records')[0]
    return data