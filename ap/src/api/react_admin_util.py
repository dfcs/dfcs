"""
Author: Howard Ye
Description: I parse the request args form the react-admin
"""
from flask import make_response, render_template, current_app
# from flask_security import current_user
from src.db import query_to_df, query_to_dfs, execute_sqls, db_session, execute_sql, jinjasql_to_df
from pandas import DataFrame
from typing import Tuple
from datetime import datetime
import json
import os
import logging
from src.db.sql_adaptor import JinjaSqlAdaptor

LOGGER = logging.getLogger('dfap')


class Audit(object):
    """
        # audit object
        # action, 
        # params: {}, 
        # t_id:[], 
        # before: {},
        # after: {}, 
        # type: str, response: {}, result: str

            Executor: '${user_email}'
        TITLE: [2020/08/12T21:35:06] The job_id [87872237] Do '${action}'
        TO: ${u1},${u2}
        BODY:
            Execution_dttm: $'{time}'
            Queue ID: '${que_id}'
            Job ID:	'${job_id}'
            Job Name:	'${job_name}'
            Job Flow Name:	'${job_flow_name}'
            Job Sche Dttm:	'${que_next_exec_dttm}'
            Data Start Dttm: ${que_exec_dttm}'
    """

    def __init__(self, executor, action, params: {}, t_ids: [], before: {} = {}, after: {} = {}, t_type: str = None, response: {} = None, result: str = None):
        self.executor = executor
        self.action = action
        self.params = params
        self.updparams = {} if (('info' not in params['body']) or (
            len(params['body']['info']) == 0))else params['body']['info']
        self.t_id = None if len(t_ids) == 0 else t_ids[0]
        self.t_ids = t_ids
        self.before = before
        self.after = after
        self.type = t_type
        self.response = response
        self.result = result
    # def fetech_before(self):
    #     pass
    # def fetch_after(self):
    #     pass
    # def set_result(self,result,response):
    #     self.result = result
    #     self.response = response


class ReactAdminListReq(object):
    def __init__(self, req, mapping_key: dict = {}, action: str = 'Query', t_ids: [] = [], t_type="QUE", executor=None):
        """
        Description: wrap Flask request
        @param req: flask reuest
        @param mapping_key: overwrite or create new kv pair for execute sql
        @param action: for auditor,action like rerun/bypass/suspend/cancel
        @param t_ids: target queue id or job id that will be effect after the action.
        @param t_type: target type
        """

        self.start = int(req.args.get('_start')) if req.args.get(
            '_start') else None
        self.end = int(req.args.get('_end')) if req.args.get('_end') else None
        self.order = req.args.get('_order')
        self.sort = req.args.get('_sort')
        # self.id = [int(x) for x in req.args.getlist('id')]
        self.id = req.args.getlist('id')
        self.wildcard_enable = False if req.args.get(
            'wildcard_enable') is None or req.args.get('wildcard_enable') == 'false' else True
        self.wildcard = False if req.args.get('wildcard_enable') is None or req.args.get(
            'wildcard_enable') == 'false' else True
        if req.method in ('POST', 'PUT'):
            if len([x for x in req.files.keys()]) == 0:
                self.body = req.json  # req.get_json(force=True)
                self.forms = req.form
            else:
                self.body = None
                self.forms = req.form
                self.files = req.files
        else:
            self.body = None
            self.forms = req.form

        self.limit = 0 if self.end is None else (self.end - self.start)
        self.offset = self.start
        self.mapping_key = mapping_key
        self.args = req.args
        self.filters = self._filters(req.args, self.mapping_key)
        # 處理UI上狀態多選
        self.job_status = req.args.getlist('job_status')
        self.db_name = req.args.getlist('db_name')

        # Dashboard上面看是要用什麼角度來查詢問題，
        # 預設是依排程角度(Sche)
        # Debug Mode是依資料角度(Data)
        self.dttm_type = req.args.get('dttm_type', 'Sche')
        self.query_dttm = req.args.get('query_dttm', '')

        if len(self.job_status) > 0 and self.job_status is not None:
            self.filters.append({'key': 'job_status', 'value': str(
                self.job_status).replace('[', '').replace(']', '')})

        if len(self.db_name) > 0 and self.db_name is not None:
            for element in self.filters:
                if element['key'] == 'db_name':
                    self.filters.remove(element)
            self.filters.append({'key': 'db_name', 'value':
                                 self.db_name})

        final_executor = executor
        # if hasattr(current_user,'email'):
        #     final_executor = current_user.email
        # elif final_executor is None:
        #     final_executor = 'anonymous@webcomm.com.tw'

        self.audit = Audit(
            executor=final_executor, action=action, params={"filters": self.filters, "body": {} if self.body is None else self.body}, t_ids=t_ids, t_type=t_type, result='N')

    def audit_set_response(self, response):
        self.audit.response = response

    def audit_set_result(self, result):
        self.audit.result = result

    def write_audit_log(self):
        LIST_SQL_DATA = render_template(
            'audit/audit_log_create.sql',
            audit=self.audit)
        # LIST_SQL_DATA=[x.strip() for x in LIST_SQL_DATA if x.strip()!='']
        self._dump_sqls('AUDITOR SQL INSERT', [LIST_SQL_DATA])
        rs_data = execute_sql(LIST_SQL_DATA, getrs=True)

        self.audit.id = rs_data[0]['audit_id']
        db_session.commit()
        # return df_data

    def upd_audit_jcs_api_result(self):
        LIST_SQL_DATA = render_template(
            'audit/audit_upd_jcs_result.sql',
            audit=self.audit)
        # LIST_SQL_DATA=[x.strip() for x in LIST_SQL_DATA if x.strip()!='']
        self._dump_sqls('AUDITOR SQL UPDATE', [LIST_SQL_DATA])
        execute_sql(LIST_SQL_DATA)
        db_session.commit()

    def _query_str_replace(self, s):
        new_s = s.replace("'", "''").replace("%", "%%")
        return new_s

    def _filters(self, req_args, mapping_key):
        return [
            {"key": mapping_key.get(x, x), "value": self._query_str_replace(req_args.get(x))} for x in req_args.keys() \
                if x not in ['_start', '_end', '_order', '_sort', 'id', 'job_status', 'dttm_type', 'query_dttm', 'wildcard_enable']
        ]
        # return [{"key":mapping_key.get(x,x),"value":req_args.get(x)} for x in req_args.keys() if x not in ['_start','_end','_order','_sort','id','job_status','dttm_type','query_dttm'] ]

    def get_value(self, key):
        return self.args.get(key)

    def body_field_fetcher(self, fields: [], ftype='str') -> [{}]:
        """
        I will return the body json to [{"key":$KEY,"valule":$VALUE}] form.
        and convert the type
        fields: the array of field string
        ftype: str|date|json
        """
        if ftype == 'date':
            return [{"key": x, "value": datetime.fromtimestamp(self.body[x]/1000.00)}
                    for x in self.body.keys() if x in fields]
        elif ftype == 'json':
            return [{"key": x, "value": json.dumps(self.body[x])}
                    for x in self.body.keys() if x in fields]
        elif ftype == 'int':
            return [{"key": x, "value": int(self.body[x])}
                    for x in self.body.keys() if x in fields]
        else:
            return [{"key": x, "value": self.body[x]}
                    for x in self.body.keys() if x in fields]

    def id_create(self,
                  template,
                  ra_id_value,
                  ra_id_name: str = 'ra_id',
                  ra_modify: {} = {},
                  ra_modify_meta: {} = {"job_mod_flag": "M",
                                        "job_mod_userid": "eu2122",
                                        "job_mod_dttm": ""}) -> DataFrame:
        """
        template(str): sql template
        ra_id_name: template where condition id name ,ex: ra_id, ra_flow_name
        ra_id_value: template  where condition id value 
        ra_modify: update fields
        ra_modify_meta: job_mod_flag,job_mod_userid,job_mod_dttm,
             if ra_modify_meta is None then we using default value
        """

        LIST_SQL_DATA = render_template(
            template,
            ra_id_name=ra_id_name,
            ra_id_value=ra_id_value,
            ra_modify=ra_modify,
            ra_modify_meta=ra_modify_meta).split(';')
        LIST_SQL_DATA = [x.strip() for x in LIST_SQL_DATA if x.strip() != '']
        self._dump_sqls('MODIFY SQL QUERY', LIST_SQL_DATA)
        df_data = jinjasql_to_df(LIST_SQL_DATA)[0]
        return df_data

    def id_modify_dep(self,
                      template,
                      ra_id_value,
                      ra_id_name: str,
                      ra_modify: {},
                      ra_modify_meta) -> DataFrame:
        """update dep"""
        LIST_SQL_DATA = render_template(
            template,
            ra_id_name=ra_id_name,
            ra_id_value=ra_id_value,
            ra_modify=ra_modify,
            ra_modify_meta=ra_modify_meta,
            ra_query=False).split(';')
        LIST_SQL_DATA = [x.strip() for x in LIST_SQL_DATA if x.strip() != '']
        self._dump_sqls('MODIFY SQL QUERY', LIST_SQL_DATA)
        # df_data = jinjasql_to_df(LIST_SQL_DATA)[0]
        execute_sqls(LIST_SQL_DATA)

    def id_modify(self,
                  template,
                  ra_id_value,
                  ra_id_name: str,
                  ra_modify: {},
                  ra_modify_meta) -> DataFrame:
        """
        template(str): sql template
        :param ra_id_name: template where condition id name ,ex: ra_id, ra_flow_name
        :param ra_id_value: template  where condition id value 
        :param ra_modify: update fields
        :param ra_modify_meta: job_mod_flag,job_mod_userid,job_mod_dttm,
             if ra_modify_meta is None then we using default value
        """

        LIST_SQL_DATA = render_template(
            template,
            ra_id_name=ra_id_name,
            ra_id_value=ra_id_value,
            ra_modify=ra_modify,
            ra_modify_meta=ra_modify_meta,
            ra_query=True).split(';')
        LIST_SQL_DATA = [x.strip() for x in LIST_SQL_DATA if x.strip() != '']
        self._dump_sqls('MODIFY SQL QUERY', LIST_SQL_DATA)
        df_data = jinjasql_to_df(LIST_SQL_DATA)[0]
        return df_data

    def _dump_sqls(self, title, list_sql_data):
        LOGGER.info(
            f'\n============{title}:{len(list_sql_data)}============\n====SQL DUMPS BEGIN===\n===========>\n{(os.linesep+os.linesep+"===========>"+os.linesep).join(list_sql_data)}\n====SQL DUMPS END===:')

    def id_sql_query(self, template, ra_id_value: str) -> DataFrame:
        # SQL_DATA= render_template(template,ra_id_value=ra_id_value).replace('\n',' ')
        SQL_DATA = render_template(template, ra_id_value=ra_id_value)
        self._dump_sqls("ID SQL QUERY", [SQL_DATA])
        df_data = jinjasql_to_df(SQL_DATA)[0]
        return df_data

    def ac_sql_query(self, template, path: str) -> Tuple[int, DataFrame]:
        """
        Autocomplete : I will create  ra_ac for jinja2 template.
        ra_ac:{"key":path,"value":get_value("q")}
        template(str): the autocomplete template path
        path(str): the autocomplete api path

        Return:
        Tuple(int,Dataframe): lenght, dataframe
        """
        if self.get_value("id") is not None:
            ac_json = {"ra_ac":
                       {"key": path,
                        "value": self.get_value("id").replace("'", "''").replace("%", "%%")}
                       # "value":self.get_value("q") if self.get_value("q") !=None else ""}
                       }
        elif self.get_value("q") is not None:
            ac_json = {"ra_ac":
                       {"key": path,
                        "value": self.get_value("q").replace("'", "''").replace("%", "%%")}
                       # "value":self.get_value("q") if self.get_value("q") !=None else ""}
                       }
        else:
            ac_json = {"ra_ac":
                       {"key": path,
                        "value": None}
                       # "value":self.get_value("q") if self.get_value("q") !=None else ""}
                       }

        len, df_data = self.list_sql_query(
            template, query_json=ac_json, is_query_total=False)
        """
        ["id","name"]
        """
        return len, df_data

    def list_sql_query(self, template_postfix,
                       query_json: {} = None,
                       is_query_total=True,
                       query_black_list: [] = [],
                       db_setting='DFCS_DB') -> Tuple[int, DataFrame]:
        """
        I will return the length of query, and data of the limit and offset.
        if the template has multiple sql statement, I will split it by ';',
        and return the result.
        ex: template_postfix=dfeg/jobmnt/list.sql
        will search
            src/api/templates/dfeg/jobmnt/list.sql 
        if is_query_total is True:
            len = len(execsql (list_total_count.sql))

        :param template_prefix(str): template file prefix without .sql
        query_json({}): define your own template variable
        :param is_query_total(bool): if count the length
        :param query_black_list({}): list the black list that will remove from request.args
        :param is_transaction(bool): if there is multiple sql statements ,enable tranaction?  
        Return: (len,df)
        """
        query_key = {"ra_filters":
            [x for x in self.filters if x["key"] not in query_black_list]}
        if query_json != None:
            # query_json = {k: self._query_str_replace(v) for k, v in query_json.items()}
            query_key.update(query_json)
        # if query_json == None:
        #     query_key = {"ra_filters":
        #                  [x for x in self.filters if x["key"] not in query_black_list]}

        # else:
        #     query_key = query_json
            # query_key = { x:self.args.get(x) for x in req_keys}
        # Setting the default value
        query_key["ra_islength"] = False
        query_key["ra_sort"] = self.sort
        query_key["ra_order"] = self.order
        query_key["ra_limit"] = self.limit
        query_key["ra_offset"] = self.offset
        query_key["ra_dttm_type"] = self.dttm_type
        query_key["ra_query_dttm"] = self.query_dttm
        query_key["ra_wildcard_enable"] = self.wildcard_enable

        query_key["ra_wildcard"] = self.wildcard

        result = jinjasql_to_df(template_postfix, query_key, db_setting)
        df_data = result[0]

        # If query total length
        if is_query_total:
            query_key["ra_islength"] = True
            df = jinjasql_to_df(template_postfix,query_key,db_setting)[0]
            if len(df.index) > 0:
                length = df.iloc[0,0]
            else:
                length = 0
        else:
            length = len(df_data)

        LOGGER.info(f'Len/Result:{length}/{len(df_data)}')
        return (length, df_data)


def make_react_admin_response(body, httpcode=200, count=0, islist=True):
    """
    res.set('Access-Control-Allow-Origin', '*')
    res.set('Access-Control-Allow-Credentials', 'true')
    """
    resp = make_response(body, httpcode)
    resp.headers['Content-Type'] = "application/json; charset=utf-8"
    # resp.headers['Access-Control-Allow-Origin'] = "*"
    resp.headers['Access-Control-Allow-Credentials'] = "true"
    if islist:
        resp.headers['X-Total-Count'] = count
        resp.headers['Access-Control-Expose-Headers'] = 'X-Total-Count'
    return resp
