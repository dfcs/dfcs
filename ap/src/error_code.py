"""
Author: Howard
Description: Define the exception
"""

class DFAPWebException(Exception):
    def __init__(self,message,**kwargs):
        super().__init__(message)
        self.status_code=500
        self.__dict__.update(kwargs)
        # for key,value in kwargs.items():
        #     print(f'key/v:{key}/{value}')
        #     self[key] = value

class DFAPLoginException(DFAPWebException):
    pass

class DFAPDBException(DFAPWebException):
    pass

class DFAPException(DFAPWebException):
    pass

class DFAPForbiddenException(DFAPWebException):
    def __init__(self,message,**kwargs):
        super().__init__(message)
        self.status_code=401
        self.__dict__.update(kwargs)