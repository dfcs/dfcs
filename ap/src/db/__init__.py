"""
Author: 
Description: database connection
"""
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import text
import pandas as pd
import re
import copy
from pandas import DataFrame
from src.error_code import DFAPWebException, DFAPDBException
from src.db.sql_adaptor import JinjaSqlAdaptor
from flask_sqlalchemy import SQLAlchemy
from contextlib import contextmanager
from src.util.encrypt import decrypt_message
from src.config import ENV_CONFIG


db = SQLAlchemy()

def get_engine(section_name='DFCS_DB', db_schema=None):
    db_info = copy.deepcopy(ENV_CONFIG[section_name])
    db_info['db_password'] = decrypt_message(db_info['db_password'])
    if db_schema:
        db_info['db_schema'] = db_schema

    if db_info['db_type'] == 'sqlserver17':
        db_uri = 'mssql+pyodbc://{db_user}:{db_password}@{db_host}:{db_port}/{db_database}?driver=ODBC+Driver+17+for+SQL+Server'.format(**db_info)
        engine_opts = {'fast_executemany': True}
        engine = db.create_engine(db_uri, engine_opts)
    elif db_info['db_type'] == 'postgresql':
        db_uri = 'postgresql://{db_user}:{db_password}@{db_host}:{db_port}/{db_database}'.format(**db_info)
        engine_opts = {
            'convert_unicode': True,
            'connect_args': {'options': f'-c search_path={db_info["db_schema"]} -c timezone=Asia/Taipei'},
            'pool_size': 10,
            'max_overflow': 5,
            'pool_pre_ping': True
        }
        engine = db.create_engine(db_uri, engine_opts)
    return engine

engine = get_engine()

###db_session for  autocommit is False
db_session = scoped_session(sessionmaker(autocommit=False,
    autoflush=False,bind=engine))

###db_session for  autocommit is True
db_session_transaction = scoped_session(sessionmaker(autocommit=True,
    autoflush=True,bind=engine))

@contextmanager
def session_scope(db_setting='DFCS_DB'):
    """Provide a transactional scope around a series of operations."""
    session = db_session
    session = session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()

###Start to create database model for security
Base = declarative_base()
Base.query = db_session.query_property()

def init_db():
    import src.security
    Base.metadata.create_all(bind=engine)

def table_to_df(table_name, columns=None, schema=None):
    df = pd.read_sql_table(
        table_name=table_name,
        con=engine,
        schema=schema,
        columns=columns)
    return df

def query_to_df(sql, params=None) -> DataFrame:
    try:
        session = db_session()
        df = pd.read_sql_query(sql, con=session.connection(), params=params)
    except Exception as e:
        raise DFAPDBException(e,sql=sql)
    return df

def query_to_dfs(sqls:[],  params=None) -> DataFrame :
    dfs =[]
    try:
        session = db_session()
        connection = session.connection()
        for sql in sqls:
 
            if bool(re.match('select',sql,re.I)):
 
                df = pd.read_sql_query(sql, con=connection, params=params)
 
                dfs.append(df)
            else:
                rs  = connection.execute(text(sql))
 
    except Exception as e:
        raise DFAPDBException(e)

    return dfs
    

def execute_sql(sql, params=dict(), getrs=False) -> DataFrame:
    try:
        with session_scope() as session:
            rs = session.execute(text(sql), params)
        result = []
        if getrs:
            for x in rs: 
                mydict = {}
                for column,value in x.items():
                    mydict[column]=value
                result.append(mydict)
        rs.close()
        return result
    except Exception as e:
        raise DFAPDBException(e,sql=sql)

def execute_sqls(sqls:[],params=None) -> DataFrame:
    try:
        with session_scope() as session:
            for sql in sqls:
                rs = session.execute(text(sql))
                rs.close()
    except Exception as e:
        raise DFAPDBException(e,sql=sql)


def jinjasql_execute(template, params=dict(), template_home='src/templates', db_setting='DFCS_DB') -> [list]:
    adaptor = JinjaSqlAdaptor(template_home)
    query_list, bind_params = adaptor.prepare_query(template, params)

    try:
        result_list = []
        with session_scope(db_setting) as session:
            for sql in query_list:
                rs = session.execute(text(sql), bind_params)
                if rs.cursor != None:
                    data = [dict(row) for row in rs]
                    result_list.append(data)
                
        return result_list
    except Exception as e:
        raise DFAPDBException(e,sql=sql)

def jinjasql_to_df(template, params=dict(), db_setting='DFCS_DB') -> [pd.DataFrame]:
    df_list = []
    result_list = jinjasql_execute(template, params, db_setting=db_setting)
    for result in result_list:
        df = pd.DataFrame.from_records(result)
        df_list.append(df)
    
    return df_list