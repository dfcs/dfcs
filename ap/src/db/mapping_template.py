MAPPING_HOME = {
    'src/templates': 'src/templates',
    'src/etl/sqls': 'src/etl/sqls'
}

MAPPING_TEMPLATE = {
    ### Parser Templates ###
    # dep_analyzer
    'dep_analyzer/clas_cfg_schedule_plan.sql': 'dep_analyzer/clas_cfg_schedule_plan.sql',
    'dep_analyzer/clds_cfg_jobtbl_link.sql': 'dep_analyzer/clds_cfg_jobtbl_link.sql',
    'dep_analyzer/clmd_cfg_tabledep_from_clds.sql': 'dep_analyzer/clmd_cfg_tabledep_from_clds.sql',
    'dep_analyzer/clmd_cfg_tabledep.sql': 'dep_analyzer/clmd_cfg_tabledep.sql',
    'dep_analyzer/dfeg_cfg_jobtbl_link.sql': 'dep_analyzer/dfeg_cfg_jobtbl_link.sql',
    'dep_analyzer/dfeg_cfg_schedule_plan.sql': 'dep_analyzer/dfeg_cfg_schedule_plan.sql',
    'dep_analyzer/dfmd_cfg_tabledep.sql': 'dep_analyzer/dfmd_cfg_tabledep.sql',
    'dep_analyzer/set_jobinfo_schedule.sql': 'dep_analyzer/set_jobinfo_schedule.sql',
    
    # transform
    'transform/dfeg_cfg_flowinfo.sql': 'transform/dfeg_cfg_flowinfo.sql',
    'transform/dfeg_cfg_jobdep.sql': 'transform/dfeg_cfg_jobdep.sql',
    'transform/dfmd_cfg_columninfo.sql': 'transform/dfmd_cfg_columninfo.sql',
    'transform/dfmd_cfg_databaseinfo.sql': 'transform/dfmd_cfg_databaseinfo.sql',
    'transform/dfmd_cfg_schemainfo.sql': 'transform/dfmd_cfg_schemainfo.sql',
    'transform/dfmd_cfg_tableinfo.sql': 'transform/dfmd_cfg_tableinfo.sql',
    'transform/dfmd_log_table_statistic.sql': 'transform/dfmd_log_table_statistic.sql',

    # workflow
    'workflow/parser_check_prerequisite.sql': 'workflow/parser_check_prerequisite.sql',
    'workflow/parser_log_insert.sql': 'workflow/parser_log_insert.sql',
    'workflow/parser_log_update.sql': 'workflow/parser_log_update.sql',

    # common
    'get_syscode_data.sql': 'get_syscode_data.sql',


    ### API Templates ###
    # data_dep_discover page
    'assets/data_dep_discover/data_dep_discover_cjb.sql': 'assets/data_dep_discover/data_dep_discover_cjb.sql',
    'assets/data_dep_discover/data_dep_discover_table.sql': 'assets/data_dep_discover/data_dep_discover_table.sql',
    
    # jobs page
    'assets/jobs/cjb_info.sql': 'assets/jobs/cjb_info.sql',
    'assets/jobs/columns.sql': 'assets/jobs/columns.sql',
    'assets/jobs/get_sqls.sql': 'assets/jobs/get_sqls.sql',
    'assets/jobs/job_info.sql': 'assets/jobs/job_info.sql',
    'assets/jobs/jobs.sql': 'assets/jobs/jobs.sql',
    'assets/jobs/process_log_details.sql': 'assets/jobs/process_log_details.sql',
    'assets/jobs/process_log.sql': 'assets/jobs/process_log.sql',
    'assets/jobs/sche_jobs.sql': 'assets/jobs/sche_jobs.sql',
    'assets/jobs/table_info.sql': 'assets/jobs/table_info.sql',

    # tables page
    'assets/tables/cjb_info.sql': 'assets/tables/cjb_info.sql',
    'assets/tables/column_details.sql': 'assets/tables/column_details.sql',
    'assets/tables/columns.sql': 'assets/tables/columns.sql',
    'assets/tables/job_info.sql': 'assets/tables/job_info.sql',
    'assets/tables/process_log_details.sql': 'assets/tables/process_log_details.sql',
    'assets/tables/process_log.sql': 'assets/tables/process_log.sql',
    'assets/tables/table_info.sql': 'assets/tables/table_info.sql',
    'assets/tables/tables.sql': 'assets/tables/tables.sql',

    # data_asset page
    'assets/data_asset.sql': 'assets/data_asset.sql',

    # data_usage_logs page
    'assets/data_usage_logs.sql': 'assets/data_usage_logs.sql',

    # autocomplete
    'common/autocomplete/audit_account.sql': 'common/autocomplete/audit_account.sql',
    'common/autocomplete/audit_database_name.sql': 'common/autocomplete/audit_database_name.sql',
    'common/autocomplete/audit_table_name.sql': 'common/autocomplete/audit_table_name.sql',
    'common/autocomplete/catalog.sql': 'common/autocomplete/catalog.sql',
    'common/autocomplete/data_tag.sql': 'common/autocomplete/data_tag.sql',
    'common/autocomplete/days_of_week.sql': 'common/autocomplete/days_of_week.sql',
    'common/autocomplete/db_name.sql': 'common/autocomplete/db_name.sql',
    'common/autocomplete/flow_name.sql': 'common/autocomplete/flow_name.sql',
    'common/autocomplete/job_id.sql': 'common/autocomplete/job_id.sql',
    'common/autocomplete/job_name.sql': 'common/autocomplete/job_name.sql',
    'common/autocomplete/job_template.sql': 'common/autocomplete/job_template.sql',
    'common/autocomplete/output_flag.sql': 'common/autocomplete/output_flag.sql',
    'common/autocomplete/platform.sql': 'common/autocomplete/platform.sql',
    'common/autocomplete/project.sql': 'common/autocomplete/project.sql',
    'common/autocomplete/role_name.sql': 'common/autocomplete/role_name.sql',
    'common/autocomplete/schedule.sql': 'common/autocomplete/schedule.sql',
    'common/autocomplete/start_times.sql': 'common/autocomplete/start_times.sql',
    'common/autocomplete/status.sql': 'common/autocomplete/status.sql',
    'common/autocomplete/syscode.sql': 'common/autocomplete/syscode.sql',
    'common/autocomplete/system.sql': 'common/autocomplete/system.sql',
    'common/autocomplete/table_cname.sql': 'common/autocomplete/table_cname.sql',
    'common/autocomplete/table_name.sql': 'common/autocomplete/table_name.sql',

    # common
    'common/get_cjb_by_sjb.sql': 'common/get_cjb_by_sjb.sql',
    'common/get_cjb_by_tab.sql': 'common/get_cjb_by_tab.sql',

    # dashboard page
    'dashboard/monitor.sql': 'dashboard/monitor.sql',
    'dashboard/res_usage.sql': 'dashboard/res_usage.sql',
    
    # lineage
    'lineage/autosys_tree.sql': 'lineage/autosys_tree.sql',
    'lineage/discover_cjb_tree.sql': 'lineage/discover_cjb_tree.sql',
    'lineage/discover_table_tree.sql': 'lineage/discover_table_tree.sql',
    'lineage/flow_tree.sql': 'lineage/flow_tree.sql',
    'lineage/table_tree.sql': 'lineage/table_tree.sql',

    # systemctl
    'systemctl/account_mnt_create.sql': 'systemctl/account_mnt_create.sql',
    'systemctl/account_mnt_delete.sql': 'systemctl/account_mnt_delete.sql',
    'systemctl/account_mnt_query.sql': 'systemctl/account_mnt_query.sql',
    'systemctl/account_mnt_update.sql': 'systemctl/account_mnt_update.sql',
    'systemctl/parser_status.sql': 'systemctl/parser_status.sql',
    'systemctl/role_mnt_create.sql': 'systemctl/role_mnt_create.sql',
    'systemctl/role_mnt_delete.sql': 'systemctl/role_mnt_delete.sql',
    'systemctl/role_mnt_query.sql': 'systemctl/role_mnt_query.sql',
    'systemctl/role_mnt_update.sql': 'systemctl/role_mnt_update.sql',
}