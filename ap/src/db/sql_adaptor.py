"""
Author: Howard Ye
Description: for jinjasql adaptor
"""
from jinjasql import JinjaSql
from jinja2 import Environment
import os
import logging
from src.db.mapping_template import MAPPING_TEMPLATE, MAPPING_HOME

LOGGER  = logging.getLogger('dfap')

def jinja_like_enable(source):
    """
    Check if ra_wildcard is true
    """
    source = source.replace('_', '\_')
    return f'%{source}%'
 

def jinja_like_disable(source):
    """
    Check if ra_wildcard is true
    """
    return source

class JinjaSqlAdaptor(object):
    def __init__(self,template_home='src/templates'):
        env = Environment()
        self._env = env
        self._template_home = template_home
        self._jsql = JinjaSql(env=env,param_style='named')

    def _get_template_source(self,template):
        with open(
                os.path.join(
                    MAPPING_HOME[self._template_home],
                    MAPPING_TEMPLATE[template]),
                    'r'
            ) as source:
            content = source.read()
        return content

    def prepare_query(self, template, data, is_dumpsql=True):
        LOGGER.debug(f'into prepare_query: {template}, {data}')
        source = self._get_template_source(template)

        self._env.filters["like"] = jinja_like_enable
        LIST_SQL_DATA, BINDING_PARAMETERS = self._jsql.prepare_query(source, data)
        LIST_SQL_DATA = LIST_SQL_DATA.split(';')

        LIST_SQL_DATA=[x.strip() for x in LIST_SQL_DATA if x.strip()!='']

        self._dump_sqls('QUERY SQL DATA',LIST_SQL_DATA)
        LOGGER.info(f'BINDING_PARAMETERS: {BINDING_PARAMETERS}')
        return LIST_SQL_DATA, BINDING_PARAMETERS

    def _dump_sqls(self,title,list_sql_data):
        LOGGER.info(f'\n============{title}:{len(list_sql_data)}============\n====SQL DUMPS BEGIN===\n===========>\n{(os.linesep+os.linesep+"===========>"+os.linesep).join(list_sql_data)}\n====SQL DUMPS END===:')