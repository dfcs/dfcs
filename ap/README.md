# Backend API

## Setting config

1.add file src/config/config.ini reference src/config/config.ini.template
2.generate key by function generate_key in src/util/encrypt.py
3.update encrypt password in src/config/config.ini

```sh
# Install python package
pip install requirements.txt

# start server
./bin/start_server.sh
```

