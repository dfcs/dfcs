import * as React from "react";
import { Admin, Resource, usePermissions } from "react-admin";

import simpleRestProvider from "ra-data-simple-rest";

import "./App.css";

const App = () => {
  return (
    <Admin dataProvider={simpleRestProvider("http://path.to.my.api")}>
      <Resource name="hello"></Resource>
    </Admin>
  );
};

export default App;
