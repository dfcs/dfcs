import React, { useEffect, useState } from "react";
import { useInput, useTranslate } from "react-admin";

import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import ListItemText from "@material-ui/core/ListItemText";
import Select from "@material-ui/core/Select";
import Checkbox from "@material-ui/core/Checkbox";
import Chip from "@material-ui/core/Chip";
import Grid from "@material-ui/core/Grid";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import { useField } from "react-final-form";

const useStyles = makeStyles((theme) => {
  return {
    formControl: {
      margin: theme.spacing(1),
      minWidth: 160,
      backgroundColor: "rgba(0, 0, 0, 0.09)",
      "border-top-left-radius": 4,
      "border-top-right-radius": 4,
    },
    chips: {
      display: "flex",
      flexWrap: "wrap",
    },
    chip: {
      margin: 2,
    },
    noLabel: {
      marginTop: theme.spacing(3),
    },
    select: {
      paddingLeft: theme.spacing(1),
      "&.MuiInput-formControl": {
        width: "100%",
      },
      "&.Mui-focused": {
        backgroundColor: "rgba(0, 0, 0, 0)",
      },
      "& div.MuiSelect-select:focus": {
        backgroundColor: "rgba(0, 0, 0, 0)",
      },
    },
  };
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const CheckSelect = (props) => {
  return (
    <FormControl variant="filled" className={classes.formControl}>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          paddingRight: theme.spacing(1),
        }}
      >
        {/* <Grid container>
        <Grid item> */}
        <div style={{ flex: 1 }}>
          <InputLabel id="demo-simple-select-filled-label">
            {props.label}
          </InputLabel>
          <Select>
            {choice.map((name) => (
              <MenuItem key={name.id} value={name.id}>
                <Checkbox checked={select_data.indexOf(name.id) > -1} />
                <span>{name.id}</span>
              </MenuItem>
            ))}
          </Select>
        </div>
        <div
          style={{ cursor: "pointer" }}
          onClick={() => {
            if (select_data.length > 0) {
              input.onChange([]);
              setSelectData([]);
            }
          }}
        >
          <HighlightOffIcon />
        </div>
      </div>
    </FormControl>
  );
};

export default CheckSelect;
