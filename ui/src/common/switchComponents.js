import React, { useState, useEffect } from "react";
import {
  AutocompleteInput,
  BooleanInput,
  TextInput,
  TextField,
  ReferenceInput,
  SelectInput,
  ReferenceArrayInput,
  SelectArrayInput,
  FunctionField,
  useTranslate,
  DateField,
  DeleteButton,
  useDataProvider,
  useRefresh,
} from "react-admin";

import {
  DateInput,
  DateTimeInput,
  TimeInput,
} from "../Components/Cell/DateTimeInput";
import {
  useTheme,
  Select,
  FormControl,
  InputLabel,
  Button,
  IconButton,
} from "@material-ui/core";

import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";

import ArrField from "../Components/Cell/ArrField";
import StatusLight from "../Components/Cell/StatusLight";

import { DLButton } from "../Components/DL";
import ExpandView from "../Components/ExpandView";

import SelectMultiInput from "./SelectMultiInput";

const EditableTextField = ({ record, source, ...props }) => {
  const [edit_mode, setEditMode] = useState(false);
  const t = useTranslate();
  const refresh = useRefresh();

  const { resource } = props;
  const [select_info, setSelectInfo] = useState([]);
  const [selected, setSelected] = useState(null);
  const [init_data, setInit] = useState(null);
  const dataProvider = useDataProvider();
  console.log("props record:", record);

  return (
    <div
      onDoubleClick={() => {
        if (!edit_mode) setEditMode(!edit_mode);
      }}
    >
      {!edit_mode ? (
        record["rolename"]
      ) : (
        <>
          <FormControl style={{ maxWidth: "100%" }}>
            <InputLabel htmlFor="age-native-simple">角色</InputLabel>
            <Select>
              <option value={null}>{""}</option>
              {select_info.map((s) => (
                <option value={s.id}>{s.name}</option>
              ))}
            </Select>
          </FormControl>
          <IconButton>
            <CheckIcon />
          </IconButton>
          <IconButton>
            <CloseIcon />
          </IconButton>
        </>
      )}
    </div>
  );
};

const SwitchComponents = ({ source = "", ...props }) => {};

const SwitchMaterialComponents = ({ value, source = "", ...props }) => {
  const data_type = source.split("_").pop();
};

const SwitchFilterComponents = ({}) => {};

const SwitchExpandComponents = ({ ...props }) => {
  return <ExpandView />;
};

export {
  SwitchComponents,
  SwitchMaterialComponents,
  SwitchFilterComponents,
  SwitchExpandComponents,
};
