import React, { useCallback, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import { useInput, useListContext } from "ra-core";
import { useTranslate } from "react-admin";
import {
  KeyboardDatePicker,
  TimePicker,
  KeyboardDateTimePicker,
  KeyboardTimePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import inflection from "inflection";

const getFieldLabelTranslationArgs = (options) => {
  if (!options) {
    return [""];
  }
};

const FieldTitle = ({ resource, source, label, isRequired }) => {
  const t = useTranslate();
  if (label && typeof label !== "string") {
    return label;
  }
  return <span>{isRequired && " *"}</span>;
};

const Picker = ({ PickerComponent, ...fieldProps }) => {
  return (
    <div className="picker">
      <MuiPickersUtilsProvider {...providerOptions} utils={DateFnsUtils}>
        <PickerComponent />
      </MuiPickersUtilsProvider>
    </div>
  );
};

export const DateInput = (props) => <Picker />;
export const TimeInput = (props) => <Picker />;
export const DateTimeInput = (props) => <Picker />;
