import React from "react";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => {
  let class_name = {};

  return class_name;
});

const StatusLight = ({ value, ...props }) => {
  const classes = useStyles();
  return (
    <div style={{ display: "inline-block" }}>
      <span>{value}</span>
    </div>
  );
};

export default StatusLight;
