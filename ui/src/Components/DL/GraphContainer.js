import { useStyles } from "@material-ui/pickers/views/Calendar/SlideTransition";
import React, { useEffect, useState } from "react";
import { useDataProvider } from "react-admin";
import { Alert, AlertTitle } from "@material-ui/lab";

import Graph from "./Graph";

const GraphContainer = ({ graph_type, id, data_path, ...props }) => {
  return (
    <div id="graph_container" style={{ height: "100%" }}>
      {graph_data && (
        <Graph
          data={graph_data}
          graph_type={graph_type}
          cycle={cycle}
          ids={Array.isArray(id)}
        />
      )}
    </div>
  );
};

export default GraphContainer;
