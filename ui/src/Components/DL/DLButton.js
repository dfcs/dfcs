import React from "react";
import { IconButton } from "@material-ui/core";

import BubbleChartIcon from "@material-ui/icons/BubbleChart";

import { Link } from "react-router-dom";

const DLButton = ({ cod_type, source, ...props }) => {
  return (
    <>
      {!disabled ? (
        <Link>
          <IconButton
            disabled={disabled}
            color="primary"
            aria-label="show diagram"
          >
            <BubbleChartIcon />
          </IconButton>
        </Link>
      ) : (
        <IconButton
          disabled={disabled}
          color="primary"
          aria-label="show diagram"
        >
          <BubbleChartIcon />
        </IconButton>
      )}
    </>
  );
};

export default DLButton;
