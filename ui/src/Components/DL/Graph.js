import React, { useEffect, useRef, useState } from "react";
import * as _d3 from "./d3";
import _d3Tip from "d3-tip";
import { dsv, event as currentEvent } from "d3";
import { Alert, AlertTitle } from "@material-ui/lab";
import uuid from "react-uuid";
import { connect } from "react-redux";

import { lightTheme } from "../../Layout/Theme";
import { trimLongText } from "../../common/util";
import { withRouter } from "react-router";
import { CRUD_CHANGE_LIST_PARAMS } from "react-admin";
import "./graph.css";

const d3 = { ..._d3, tip: _d3Tip };
Array.prototype.remove = function () {
  var what,
    a = arguments,
    L = a.length,
    ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};

const Graph = ({ data, isSingle = false, dispatch, ids = [], ...props }) => {
  const d3_container = useRef(null);
  const [cycle, setCycle] = useState(false);
  const [focus_element, setFocusElement] = useState(null);
  const uid = uuid();
  console.log("graph data:", data);

  return (
    <div style={{ height: "100%" }} onClick={closeNode}>
      {
        <svg
          width="100%"
          height="100%"
          style={{ minHeight: 300 }}
          ref={d3_container}
        >
          <g className={`doodle-${uid}`}></g>
        </svg>
      }
      {cycle && (
        <Alert severity="error">
          <AlertTitle>Error</AlertTitle>
          This is an error cycle
        </Alert>
      )}
    </div>
  );
};

export default withRouter(connect()(Graph));
