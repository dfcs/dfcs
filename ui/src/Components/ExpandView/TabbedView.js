import React, { useEffect, useState } from "react";
import {
  ArrayField,
  SingleFieldList,
  TabbedShowLayout,
  Tab,
  TextField,
  ChipField,
  SimpleShowLayout,
  useTranslate,
  useDataProvider,
  useListContext,
} from "react-admin";
import SwipeableViews from "react-swipeable-views";

import ArrField from "../Cell/ArrField";
import { SwitchComponents } from "../../common/switchComponents";
import ChartGrid from "../Grids/ChartGrid";
import Data2LineChart from "../Charts/Data2LineChart";
import ListView from "./ListView";
import DiagramContainer from "../DL/GraphContainer";
import LINKS from "../../api/endpoints";
import Loading from "../General/Loading";
import SliderView from "./SliderView";
import Pagination from "./Pagination";
const Swiper = ({ content, parent, ...props }) => {
  const [index, setIndex] = useState(0);

  return (
    <div>
      <SwipeableViews>
        {content.map((c) => (
          <SliderView />
        ))}
      </SwipeableViews>
      <Pagination />
    </div>
  );
};

const TabContent = () => {
  return (
    <div>
      <SimpleShowLayout>{}</SimpleShowLayout>
    </div>
  );
};

const TabbedView = ({ tabs_name, ...props }) => {
  if (loading) return <></>;
  return (
    <TabbedShowLayout {...props}>
      {tabs_name.map((name, tab_key) => {
        return (
          <Tab>
            <TabContent />
          </Tab>
        );
      })}
    </TabbedShowLayout>
  );
};

export default TabbedView;
