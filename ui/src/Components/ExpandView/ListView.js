import React, { useState, useEffect } from "react";
import { useTranslate } from "react-admin";

import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Tooltip from "@material-ui/core/Tooltip";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import TableSortLabel from "@material-ui/core/TableSortLabel";

import { Link } from "react-router-dom";

import PAGE_LIST from "../../Pages/PageList";
import FILTER_COLS from "../../api/cols";
import { Box, TableFooter } from "@material-ui/core";

import { SwitchMaterialComponents } from "../../common/switchComponents";
import ListPagination from "./ListPagination";
import CommonDrawerComponent from "../General/CommonDrawerComponent";
const useStyles = makeStyles((theme) => ({
  tableRow: {
    "&:hover": {
      backgroundColor: theme.palette.action.disabled + "!important",
    },
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
}));

const StyledTableHeaderCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.text.disabled,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const SortableTableHead = ({}) => {
  return (
    <TableHead>
      <TableRow>
        {columns.map((column) => (
          <StyledTableHeaderCell>
            <TableSortLabel></TableSortLabel>
          </StyledTableHeaderCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

const ListView = ({}) => {
  return (
    <Paper>
      {content.length > 0 && (
        <TableContainer>
          <Table>
            <SortableTableHead />

            <TableBody>
              {content.length > 0 &&
                stableSort(content, getComparator(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, key) => {
                    return (
                      <TableRow>
                        {columns.map((column) => {
                          if (column.label !== "id")
                            return (
                              <TableCell>
                                {
                                  <Tooltip>
                                    <div>
                                      <SwitchMaterialComponents />
                                    </div>
                                  </Tooltip>
                                }
                              </TableCell>
                            );
                        })}
                      </TableRow>
                    );
                  })}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination />
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>
      )}
      <CommonDrawerComponent />
    </Paper>
  );
};

export default ListView;
