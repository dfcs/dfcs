import React from "react";

const styles = {
  root: {
    // position: "absolute",
    bottom: 8,
    right: 8,
    display: "flex",
    flexDirection: "row",
  },
};

const Dotstyles = {
  root: {
    height: 18,
    width: 18,
    cursor: "pointer",
    border: 0,
    background: "none",
    padding: 0,
  },
  dot: {
    backgroundColor: "white",
    border: "1px solid black",
    height: 12,
    width: 12,
    borderRadius: 6,
    margin: 3,
  },
  active: {
    // #319fd6
    backgroundColor: "black",
  },
};

class PaginationDot extends React.Component {
  handleClick = (event) => {
    this.props.onClick(event, this.props.index);
  };

  render() {
    const { active } = this.props;

    let styleDot;

    if (active) {
      styleDot = Object.assign({}, Dotstyles.dot, Dotstyles.active);
    } else {
      styleDot = Dotstyles.dot;
    }

    return (
      <button type="button" style={Dotstyles.root} onClick={this.handleClick}>
        <div />
      </button>
    );
  }
}

class Pagination extends React.Component {
  handleClick = (event, index) => {
    this.props.onChangeIndex(index);
  };

  render() {
    const { index, dots } = this.props;

    const children = [];

    for (let i = 0; i < dots; i += 1) {
      children.push(<PaginationDot />);
    }

    return <div style={styles.root}>{children}</div>;
  }
}

export default Pagination;
