import React, { useEffect, useState } from "react";
import { useDataProvider } from "react-admin";
import TabbedView from "./TabbedView";

const ExpandView = ({ ...props }) => {
  const { resource } = props;
  const tabs_env = process.env[`REACT_APP_${resource.toUpperCase()}_TABS`];
  const TABS = tabs_env ? tabs_env.split(",") : [];

  return <TabbedView />;
};

export default ExpandView;
