import React, { useState, useEffect } from "react";
import Drawer from "@material-ui/core/Drawer";
import { SimpleShowLayout, TextField, useDataProvider } from "react-admin";
import { SwitchComponents } from "../../common/switchComponents";

const CommonDrawer = ({}) => {
  return (
    <Drawer>
      <div>
        <SimpleShowLayout></SimpleShowLayout>
      </div>
    </Drawer>
  );
};

export default CommonDrawer;
