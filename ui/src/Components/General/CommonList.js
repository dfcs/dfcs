import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core";
import { Fragment } from "react";
import Tooltip from "@material-ui/core/Tooltip";

import {
  CreateButton,
  TopToolbar,
  List,
  BulkDeleteButton,
  useListContext,
} from "react-admin";
import Datagrid from "./CustDatagrid/Datagrid";
import DatagridBody from "./CustDatagrid/DatagridBody";
import { SwitchComponents } from "../../common/switchComponents";

import CommonFilter from "./CommonFilter";
import CommonExpand from "./CommonExpand";

const useStyles = makeStyles({
  Header: {
    backgroundColor: "#cfcfcf",
  },
});

const DeleteBulkActionButtons = (props) => (
  <Fragment>
    <BulkDeleteButton />
  </Fragment>
);

const ListActions = (props) => {
  return <TopToolbar className={className}></TopToolbar>;
};

const CommonList = ({}) => {
  const classes = useStyles();

  return (
    <List>
      <Datagrid>
        {list_cols &&
          list_cols.map((src, key) => {
            return <SwitchComponents />;
          })}
      </Datagrid>
    </List>
  );
};

export default CommonList;
