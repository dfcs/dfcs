import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { TOGGLE_LIST_ITEM_EXPAND } from "react-admin";

import { SwitchExpandComponents } from "../../common/switchComponents";

const CommonExpand = ({ dispatch, ...props }) => {
  return <SwitchExpandComponents />;
};

export default connect()(CommonExpand);
