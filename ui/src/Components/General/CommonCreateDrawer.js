import React, { useState, useEffect } from "react";
import Drawer from "@material-ui/core/Drawer";
import {
  SimpleForm,
  TextField,
  useDataProvider,
  Create,
  useRefresh,
  useTranslate,
} from "react-admin";
import { SwitchFilterComponents } from "../../common/switchComponents";

const CommonCreateDrawer = ({}) => {
  return (
    <Drawer>
      <div>
        <Create>
          <SimpleForm></SimpleForm>
        </Create>
      </div>
    </Drawer>
  );
};

export default CommonCreateDrawer;
