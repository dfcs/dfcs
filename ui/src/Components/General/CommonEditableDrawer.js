import React, { useState, useEffect } from "react";
import Drawer from "@material-ui/core/Drawer";
import {
  SimpleForm,
  TextField,
  useDataProvider,
  Create,
  Edit,
} from "react-admin";
import { SwitchFilterComponents } from "../../common/switchComponents";

const CommonEditableDrawer = ({}) => {
  return (
    <Drawer>
      <div style={{ minWidth: "450px" }}>
        {id && (
          <Edit>
            <SimpleForm></SimpleForm>
          </Edit>
        )}
      </div>
    </Drawer>
  );
};

export default CommonEditableDrawer;
