import React, { useEffect } from "react";
import { connect } from "react-redux";

import {
  Filter,
  useListContext,
  CRUD_CHANGE_LIST_PARAMS,
  CRUD_GET_LIST,
} from "react-admin";
import { useField } from "react-final-form";

import { SwitchFilterComponents } from "../../common/switchComponents";
import { tree } from "d3";

const CommonFilter = ({}) => {
  return (
    <Filter>
      {cols &&
        cols.length > 0 &&
        cols.map((col, key) => {
          return <SwitchFilterComponents />;
        })}
      {append_cols.length > 0 &&
        append_cols.map((col, key) => {
          return <SwitchFilterComponents />;
        })}
    </Filter>
  );
};

export default connect()(CommonFilter);
