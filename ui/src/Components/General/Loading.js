import React from "react";
import ReactLoading from "react-loading";

const Loading = (props) => {
  return (
    <div
      style={{
        backgroundColor: "rgba(0,0,0,0)",
        position: "fixed",
        zIndex: 1301,
        display: "flex",
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
        width: "100%",
      }}
    >
      <ReactLoading type={"bars"} color="#2196f3" />
    </div>
  );
};

export default Loading;
