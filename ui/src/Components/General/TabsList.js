import React, { useState, useEffect } from "react";
import { Card, makeStyles, useTheme } from "@material-ui/core";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import { Datagrid, List, useListContext, useTranslate } from "react-admin";
import { SwitchComponents } from "../../common/switchComponents";
import DiagramContainer from "../DL/GraphContainer";

import CommonFilter from "./CommonFilter";
import CommonExpand from "./CommonExpand";

const useStyles = makeStyles({
  Header: {
    backgroundColor: "#cfcfcf",
  },
});

const TabsDataGrid = ({}) => {
  return (
    <Card>
      <Tabs>
        {tabs.map((tab) => (
          <Tab label={t(`resources.${props.resource}.fields.${tab}`)} />
        ))}
      </Tabs>
      <Card></Card>
      <Datagrid>
        {list_cols &&
          list_cols.map((src, key) => {
            const { data } = props;
            return <SwitchComponents />;
          })}
      </Datagrid>
    </Card>
  );
};

const CommonList = ({}) => {
  return (
    <List>
      <TabsDataGrid />
    </List>
  );
};

export default CommonList;
