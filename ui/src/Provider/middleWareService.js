import { fetchUtils } from "ra-core";
import { stringify } from "query-string";
import { HttpError } from "react-admin";

export default (
  apiUrl,
  httpClient = fetchUtils.fetchJson,
  httpPost,
  httpPut,
  httpDelete
) => {
  return {};
};
