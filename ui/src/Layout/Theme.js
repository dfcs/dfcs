export const darkTheme = {
  palette: {
    type: "dark", // Switching the dark mode on is a single property value change.
  },
};

export const lightTheme = {
  palette: {
    primary: {
      main: "#1976d2",
    },
    secondary: {
      main: "#2196f3",
    },
    SUCCESS: {
      main: "#4caf50",
    },
    ERROR: {
      main: "#f44336",
    },
    WARNING: {
      main: "#9a0036",
    },
    RUNNING: {
      main: "#ffb74d",
    },
    WAITING: {
      main: "#2196f3",
    },
    WAIT: {
      main: "#2196f3",
    },
  },
};
