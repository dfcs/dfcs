import React from "react";

import { forwardRef } from "react";
import { MenuItem, ListItemIcon } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Language from "@material-ui/icons/Language";

import {
  Layout,
  Sidebar,
  AppBar,
  UserMenu,
  useLocale,
  useSetLocale,
} from "react-admin";
import { connect } from "react-redux";
import Loading from "../Components/General/Loading";
// import AppBar from "./AppBar";
import Menu from "./Menu";
import { darkTheme, lightTheme } from "./Theme";

const CustomSidebar = (props) => <Sidebar {...props} size={200} />;

const useStyles = makeStyles((theme) => ({
  menuItem: {
    color: theme.palette.text.secondary,
  },
  icon: { minWidth: theme.spacing(5) },
}));

const SwitchLanguage = forwardRef((props, ref) => {
  const locale = useLocale();
  const setLocale = useSetLocale();
  const classes = useStyles();
  return (
    <MenuItem
      ref={ref}
      className={classes.menuItem}
      onClick={() => {
        setLocale(locale === "en" ? "zh-tw" : "en");
        props.onClick();
      }}
    >
      <ListItemIcon className={classes.icon}>
        <Language />
      </ListItemIcon>
      Switch Language
    </MenuItem>
  );
});

const MyUserMenu = (props) => (
  <UserMenu {...props}>
    <MenuItem>{localStorage.getItem("username")}</MenuItem>
    {/* <SwitchLanguage /> */}
  </UserMenu>
);

const MyAppBar = (props) => <AppBar {...props} userMenu={<MyUserMenu />} />;

const DFLayout = ({ is_loading, dispatch, ...props }) => {
  return (
    <div>
      <Layout />
      {is_loading === true ? <Loading /> : ""}
    </div>
  );
};

const mapStateToProps = (state) => ({
  is_loading: state.admin.loading > 0 ? true : false,
});
export default connect(mapStateToProps)(DFLayout);
