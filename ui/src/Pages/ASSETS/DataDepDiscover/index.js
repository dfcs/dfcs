import PeopleIcon from "@material-ui/icons/People";
import DataDepDiscoverList from "./DataDepDiscoverList";

export default {
  list: DataDepDiscoverList,
  icon: PeopleIcon,
};
