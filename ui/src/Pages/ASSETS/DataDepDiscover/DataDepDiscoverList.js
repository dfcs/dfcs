import React, { Fragment } from "react";
import TabsList from "../../../Components/General/TabsList";
import { GetSessionStorage } from "../../../common/util";
import CommonDrawer from "../../../Components/General/CommonDrawer";
import { Route } from "react-router";
import { useTranslate, Pagination } from "react-admin";
import { usePermissions, useNotify } from "react-admin";

const PostPagination = (props) => <Pagination />;

const DataDepDiscoverList = (props) => {
  return (
    <Fragment>
      <TabsList />
    </Fragment>
  );
};
export default DataDepDiscoverList;
