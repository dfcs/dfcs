import React, { Fragment } from "react";
import CommonList from "../../../Components/General/CommonList";
import { GetSessionStorage } from "../../../common/util";
import CommonDrawer from "../../../Components/General/CommonDrawer";
import { Route } from "react-router";
import { usePermissions, useNotify } from "react-admin";

const JobsList = (props) => {
  return (
    <Fragment>
      <CommonList />
      <Route path={""}>{(route) => <CommonDrawer />}</Route>
    </Fragment>
  );
};
export default JobsList;
