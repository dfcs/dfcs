import PeopleIcon from '@material-ui/icons/People';
import JobsList from './JobsList'; 

export default {
  list: JobsList,
  icon: PeopleIcon,
};

