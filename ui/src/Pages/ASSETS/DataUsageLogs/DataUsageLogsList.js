import React, { Fragment } from "react";
import CommonList from "../../../Components/General/CommonList";
import { GetSessionStorage } from "../../../common/util";
import CommonDrawer from "../../../Components/General/CommonDrawer";
import { Route } from "react-router";
import { usePermissions, useNotify } from "react-admin";

const DataUsageLogsList = (props) => {
  return (
    <Fragment>
      <CommonList />
    </Fragment>
  );
};
export default DataUsageLogsList;
