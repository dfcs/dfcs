import PeopleIcon from '@material-ui/icons/People';
import DataUsageLogsList from './DataUsageLogsList'; 

export default {
  list: DataUsageLogsList,
  icon: PeopleIcon,
};

