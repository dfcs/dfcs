import React, { Fragment, cloneElement, useState, useEffect } from "react";
import CommonList from "../../../Components/General/CommonList";
import { GetSessionStorage } from "../../../common/util";
import { Route } from "react-router";

import {
  Button,
  TopToolbar,
  useListContext,
  sanitizeListRestProps,
  useTranslate,
  usePermissions,
  useNotify,
} from "react-admin";
import FilterListIcon from "@material-ui/icons/FilterList";
import LabelOffIcon from "@material-ui/icons/LabelOff";
import CommonDrawer from "../../../Components/General/CommonDrawer";
const TABLES_LIST_COLS = process.env.REACT_APP_TABLES_LIST_COL
  ? process.env.REACT_APP_TABLES_LIST_COL.split(",")
  : ["job_flow_name"];
const FILTER_COLS = process.env.REACT_APP_TABLES_FILTER_COL
  ? process.env.REACT_APP_TABLES_FILTER_COL.split(",")
  : [];

const ADVANCE_FILTER_COLS = process.env.REACT_APP_TABLES_ADVANCE_FILTER_COL
  ? process.env.REACT_APP_TABLES_ADVANCE_FILTER_COL.split(",")
  : [];

const ListActions = ({ setFilterCols, ...props }) => {
  const {
    className,
    exporter,
    filters,
    maxResults,
    filter_cols,
    fixed_icon,
    ...rest
  } = props;
  const {
    currentSort,
    resource,
    displayedFilters,
    filterValues,
    hasCreate,
    basePath,
    selectedIds,
    showFilter,
    total,
  } = useListContext();
  const t = useTranslate();
  const [filter_disabled, setDisabled] = useState(fixed_icon);
  const [filter_enable, setFilterEnable] = useState(
    filter_cols.length > 0 ? true : false
  );
  useEffect(() => {
    let flag = false;
    Object.keys(filterValues).map((k) => {
      ADVANCE_FILTER_COLS.map((f) => {
        if (f.search(k) == 0) {
          flag = true;
        }
      });
    });
    setDisabled(flag);
    if (flag) {
      setFilterEnable(true);
    }
  }, [filterValues]);

  return (
    <TopToolbar
      className={className}
      {...sanitizeListRestProps(rest)}
      style={{ width: "10%" }}
    >
      {/* {filters &&
        cloneElement(filters, {
          resource,
          showFilter,
          displayedFilters,
          filterValues,
          context: "button",
        })} */}

      {/* Add your custom actions */}
      <Button
        disabled={filter_disabled}
        onClick={() => {
          setFilterCols(filter_enable ? [] : [...ADVANCE_FILTER_COLS]);
          setFilterEnable(!filter_enable);
        }}
        label={
          filter_enable || filter_disabled
            ? t("ra.action.filter_close")
            : t("ra.action.filter")
        }
      >
        {filter_enable || filter_disabled ? (
          <LabelOffIcon />
        ) : (
          <FilterListIcon />
        )}
      </Button>
    </TopToolbar>
  );
};

const TableList = (props) => {
  // if (auth.assets[props.resource].enable) return <></>;
  return (
    <Fragment>
      <CommonList></CommonList>

      <ListActions />

      <Route path={""}>
        {(route) => {
          return <CommonDrawer />;
        }}
      </Route>
    </Fragment>
  );
};

export default TableList;
