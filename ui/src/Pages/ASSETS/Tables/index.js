import PeopleIcon from '@material-ui/icons/People';
import TablesList from './TablesList'; 

export default {
  list: TablesList,
  icon: PeopleIcon,
};

