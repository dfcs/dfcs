import React, { useEffect, useState } from "react";
import { Grid, Paper } from "@material-ui/core";
import { useDataProvider, useTranslate, useAuthenticated } from "react-admin";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import { GetSessionStorage } from "../../../common/util";
import { usePermissions, useNotify } from "react-admin";
import { Title } from "react-admin";

const useStyles = makeStyles((theme) => ({
  outer_container: {
    backgroundColor: theme.palette.action.selected,
    padding: theme.spacing(2),
    height: "calc(100vh - 100px)",
    overflowY: "auto",
  },
  inner_container: {
    backgroundColor: theme.palette.action.selected,
    padding: theme.spacing(2),
    margin: theme.spacing(1),
  },
}));

const OuterContainer = ({ ...props }) => {
  const t = useTranslate();
  const classes = useStyles();

  const { name, data, inner } = props;
  return (
    <Grid item xs={inner ? 12 : 3}>
      <Paper>
        {inner == "out" && (
          <Link>
            <div></div>
          </Link>
        )}
        {data.summary &&
          Object.keys(data.summary).map((key, idx) => {
            return key !== "db" && <Grid key={idx}>{content[key]}</Grid>;
          })}
      </Paper>
    </Grid>
  );
};

const DataAsset = ({ ...props }) => {
  return (
    <Grid container spacing={4}>
      <Title />
    </Grid>
  );
};

export default DataAsset;
