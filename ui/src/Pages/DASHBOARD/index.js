import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { CardHeader, CardContent, Typography } from "@material-ui/core";
import EventIcon from "@material-ui/icons/Event";

import {
  useDataProvider,
  usePermissions,
  useAuthenticated,
  Title,
  useTranslate,
} from "react-admin";
import { Link } from "react-router-dom";

import ChartGrid from "../../Components/Grids/ChartGrid";
import Data2StackBarChart from "../../Components/Charts/Data2StackBarChart";

import CircleProgressCard from "../../Components/Grids/CircleProgressCard";
import StatusGrid from "../../Components/Grids/StatusGrid";
import ListView from "../../Components/ExpandView/ListView";
import LINKS from "../../api/endpoints";
import Loading from "../../Components/General/Loading";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));
const Dashboard = (props) => {
  return (
    <div className={classes.root}>
      <Title />
      <Grid>
        {/* <Grid item xs={12}> */}
        {/* <Grid container spacing={2}> */}
        <Grid item xs={1}>
          <Paper>
            <IconButton>
              <EventIcon />
            </IconButton>

            <Typography></Typography>

            <div style={{ visibility: "hidden", position: "absolute" }}>
              <MuiPickersUtilsProvider>
                <KeyboardDatePicker />
              </MuiPickersUtilsProvider>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={11}>
          <Grid container spacing={2}></Grid>
        </Grid>
        {/* </Grid> */}
        {/* </Grid> */}
        <Grid item xs={7}>
          <Paper className={classes.paper}>
            <ChartGrid>{!loading && <Data2StackBarChart />}</ChartGrid>
          </Paper>
        </Grid>
        <Grid item xs={5}>
          <Paper className={classes.paper} style={{ height: "100%" }}>
            <CardHeader />
            <CardContent>
              <Grid container>
                {dashBoardInfo.sys_data.map((data, key) => {
                  return (
                    <Grid>
                      <CircleProgressCard />
                    </Grid>
                  );
                })}
              </Grid>
            </CardContent>
          </Paper>
        </Grid>

        <Grid item xs={12}>
          <ListView />
        </Grid>
      </Grid>
    </div>
  );
};

export default Dashboard;
