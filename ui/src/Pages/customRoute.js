import * as React from "react";
import { Route } from "react-router-dom";
import Sqls from "./GRAPH/SQLS";
import Dashboard from "./DASHBOARD";
import DataAsset from "./ASSETS/DataAsset";
import RoleMnt from "./SYSTEM/RoleMnt";
import ParserMnt from "./SYSTEM/ParserMnt";

export default [
  <Route />,
  <Route />,
  <Route />,
  <Route />,

  <Route path="/sqls/:id/:file?" />,
];
